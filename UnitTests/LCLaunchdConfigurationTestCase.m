//
//  UnitTests.m
//  UnitTests
//
//  Created by henrik on 10.12.22.
//  Copyright © 2022 hackmac. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "LCLaunchControl.h"
#import "LCJobConfig.h"


/** Configure this path for your own test files. */
//NSString * const PATH_TO_LAUNCHAGENT_PLIST = @"/System/Library/LaunchAgents/com.apple.AirPlayUIAgent.plist";
NSString * const PATH_TO_LAUNCHAGENT_PLIST = @"/System/Library/LaunchAgents/com.apple.weatherd.plist";


@interface LCLaunchdConfigurationTestCase : XCTestCase

@end

@implementation LCLaunchdConfigurationTestCase

- (void)testWritingConfig {
    LCJobConfig *launchConfig = [LCJobConfig launchdConfigurationWithContentsOfFile:PATH_TO_LAUNCHAGENT_PLIST];
    BOOL oldDisabledStatus = launchConfig.disabled;
    launchConfig.disabled = !oldDisabledStatus;
    XCTAssertTrue(launchConfig.isModified);
    NSError *error = nil;
    /* Saving system configuration should fail because of the write protection. */
    XCTAssertTrue(launchConfig.isReadOnly);
    XCTAssertFalse([launchConfig save:&error]);
    XCTAssertNotNil(error);
}

- (void)testStoppingProcess {
    LCJobConfig *launchConfig = [LCJobConfig launchdConfigurationWithContentsOfFile:PATH_TO_LAUNCHAGENT_PLIST];
    NSError *error = nil;
    pid_t p = [launchConfig pid:&error];
    XCTAssertNil(error);
    XCTAssertNotEqual(p, 0);

    LCLaunchControl *lc = LCLaunchControl.sharedLaunchControl;
    XCTAssertTrue([lc stopJob:launchConfig error:&error]);
    XCTAssertNil(error);
    XCTAssertTrue([lc startJob:launchConfig error:&error]);
    XCTAssertNil(error);
}

- (void)testListing {
    NSString * const label = @"com.apple.AirPlayUIAgent";
    id listing = [LCLaunchControl.sharedLaunchControl listJobWithLabel:label];
    XCTAssertNotNil(listing);
    XCTAssertEqualObjects([listing objectForKey:@"Label"], label);
}

@end
