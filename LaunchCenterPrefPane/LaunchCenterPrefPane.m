//
//  LaunchCenterPrefPane.m
//  LaunchCenterPrefPane
//
//  Created by henrik on 26.07.18.
//  Copyright © 2018 hackmac. All rights reserved.
//

#import "HWArrayMapping.h"

#import "LCConstants.h"
#import "LaunchCenterPrefPane.h"
#import "LCLaunchControl.h"
#import "LCJobConfig.h"


typedef NSMutableDictionary<LCConfigLocationKey, id> *LaunchConfiguration;
typedef NSArray<LaunchConfiguration> *LaunchConfigurationList;


/*
 There are some specific things to have to remember when creating a PreferencePane.
 Look at this article at Cocoa Dev Central "Creating a PreferencePane":
 http://cocoadevcentral.com/articles/000040.php
 */
/*
 To be able to debug your preference pane in Xcode 14.3, you need to set the following in your scheme as the launch executable:
 /System/Library/Frameworks/PreferencePanes.framework/Versions/A/XPCServices/legacyLoader-x86_64.xpc/Contents/MacOS/legacyLoader-x86_64
 And also check the box that tells it to wait for launch. When you open System Preferences and try to load your pane, the OS will launch legacyLoader.
 */


@interface LaunchCenterPrefPane () {
    NSString *_searchString;
}

@property (weak) IBOutlet NSPopover *previewPopover;
@property (weak) IBOutlet NSPopover *errorPopover;

@property (weak) IBOutlet NSPathControl *pathControl;

@property (weak) IBOutlet NSTextFinder *textFinder;
@property (weak) IBOutlet NSOutlineView *sourceList;
@property (weak) IBOutlet NSTableView *tableView;

/* Properties used for the preview popover */
@property (strong) NSString *selectedFileContent;
@property (strong) NSString *selectedFileFormat;

@property (nonatomic, strong) NSUserDefaultsController *sharedUserDefaultsController;

/// A list of directories where locations of daemons and agents are configured.
@property (strong) LaunchConfigurationList launchConfigLocations;

@property (strong) LCJobConfig *selectedConfiguration;

@property NSInteger selectedProcessTypeTag;

- (void)refreshSourceList;

- (void)applicationWillTerminate:(NSNotification *)aNotification;

- (void)enumerateGroupItemsUsingBlock:(void (NS_NOESCAPE ^)(id group, NSInteger rowId, BOOL * _Nonnull stop))block;
- (void)enumerateJobConfigsUsingBlock:(void (NS_NOESCAPE ^)(LCJobConfig * _Nonnull config, BOOL * _Nonnull stop))block;

- (BOOL)modifiedConfigurationExists;
- (void)saveAllModifiedConfigurations;

/**
 Loads all files of the given directory at <code>configPath</code> as job configurations.
 @param configPath
    A  path to a directory where all files are fread as job configurations
 @param error
    An optional reference of a value where error information will be stored
 @return
    The list of job configuration, <code>nil</code> if an error occures
 */
- (nullable NSArray<LCJobConfig *> *)jobConfigurationsWithFilesOfDirectory:(NSString *)configPath error:(out NSError * _Nullable __autoreleasing *)error;

@end


/// The global main resource bundle, used primally for accessing localized strings.
NSBundle *LCPrefPaneMainBundle = nil;


@implementation LaunchCenterPrefPane

+ (NSSet<NSString *> *)keyPathsForValuesAffectingSelectedProcessTypeTag
{
    return [NSSet setWithObject:@"selectedConfiguration"];
}


- (instancetype)initWithBundle:(NSBundle *)bundle
{
    self = [super initWithBundle:bundle];
    if (nil == self) {
        return nil;
    }
    LCPrefPaneMainBundle = bundle;

    _sharedUserDefaultsController = nil;
    _searchString = nil;

    // find sources of job configuration files form defaults and filter eventually invalid pathes
    NSDictionary<LCUserDefaultsKey, id> *defaults = [self.sharedUserDefaultsController.defaults persistentDomainForName:LCPrefPaneMainBundle.bundleIdentifier];
    assert(nil != defaults && 0 != defaults.count);
    LaunchConfigurationList locationList = [defaults objectForKey:LCUserDefaultsLaunchConfigLocationsKey];
    _launchConfigLocations = [locationList filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id _Nullable item, NSDictionary<NSString *,id> * _Nullable bindings) {
        NSString *configPath = [[item objectForKey:LCConfigLocationConfigPathKey] stringByExpandingTildeInPath];
        BOOL isDir = NO;
        return [NSFileManager.defaultManager fileExistsAtPath:configPath isDirectory:&isDir] && isDir;
    }] mappingObjects:^id(LaunchConfiguration _Nonnull obj) {
        return obj.mutableCopy;
    }];

    return self;
}


- (void)mainViewDidLoad
{
    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(applicationWillTerminate:)
                                               name:NSApplicationWillTerminateNotification
                                             object:nil];

    [self.pathControl addObserver:self
                       forKeyPath:@"objectValue"    // this is the URL property of the PathControl
                          options:(NSKeyValueObservingOptionNew)
                          context:nil];

    // initialize tree controller with all configured launchAgents and launchDaemons at the standard system paths
    // how to obtain more information from the launch configurations, see:
    //  https://github.com/emorydunn/LaunchAgent/blob/master/Sources/LaunchAgent/LaunchControl.swift
    // and
    //  https://github.com/emorydunn/LaunchAgent/blob/master/Sources/LaunchAgent/LaunchAgent%2BStatus.swift
    // Und noch etwas auf Deutsch:
    //  https://www.ifun.de/der-mac-os-systemstart-anmeldeobjekte-startup-items-launch-agents-und-co-60579/

    //[self showFindBar:nil];
}


- (void)willUnselect
{
    if ([_sharedUserDefaultsController.defaults boolForKey:LCUserDefaultsSaveOnChangePrefPaneFlagKey]) {
        // save all configurations
        [self saveAllModifiedConfigurations];
    }
}


#pragma mark - Getter-Methods


- (NSUserDefaultsController *)sharedUserDefaultsController
{
    if (nil != _sharedUserDefaultsController) {
        return _sharedUserDefaultsController;
    }

    // load the default values for the user defaults
    NSDictionary<NSString *, id> *prefs = [NSUserDefaults.standardUserDefaults persistentDomainForName:LCPrefPaneMainBundle.bundleIdentifier];
    if (nil == prefs)
    {
        // load the default values from the factory settings file
        NSString *factorySettingsPath = [[NSBundle bundleWithIdentifier:LCPrefPaneMainBundle.bundleIdentifier] pathForResource:@"FactorySettings" ofType:@"plist"];
        prefs = [NSDictionary dictionaryWithContentsOfFile:factorySettingsPath];

        // set them in the standard user defaults
        _sharedUserDefaultsController = [[NSUserDefaultsController alloc] initWithDefaults:[[NSUserDefaults alloc] initWithSuiteName:LCPrefPaneMainBundle.bundleIdentifier]
                                                                             initialValues:prefs];
        // save
        [_sharedUserDefaultsController.defaults removePersistentDomainForName:LCPrefPaneMainBundle.bundleIdentifier];
        [_sharedUserDefaultsController.defaults setPersistentDomain:prefs forName:LCPrefPaneMainBundle.bundleIdentifier];
    } else {
        // set them in the standard user defaults
        _sharedUserDefaultsController = [[NSUserDefaultsController alloc] initWithDefaults:[[NSUserDefaults alloc] initWithSuiteName:LCPrefPaneMainBundle.bundleIdentifier]
                                                                             initialValues:prefs];
    }

    [_sharedUserDefaultsController.defaults addObserver:self forKeyPath:LCUserDefaultsShouldCheckFilesFlagKey
                                                options:NSKeyValueObservingOptionNew context:nil];
    // These observer are disabled, since they have no work to do (until now).
    /*[_sharedUserDefaultsController.defaults addObserver:self forKeyPath:LCUserDefaultsSaveOnChangeFileFlagKey
                                                options:NSKeyValueObservingOptionNew context:nil];*/
    /*[_sharedUserDefaultsController.defaults addObserver:self forKeyPath:LCUserDefaultsSaveOnChangePrefPaneFlagKey
                                                options:NSKeyValueObservingOptionNew context:nil];*/
    /*[_sharedUserDefaultsController.defaults addObserver:self forKeyPath:LCUserDefaultsSaveOnQuitFlagKey
                                                options:NSKeyValueObservingOptionNew context:nil];*/

    return _sharedUserDefaultsController;
}


- (NSInteger)selectedProcessTypeTag
{
    LCProcessTypeValue type = _selectedConfiguration.processType;
    if ([LCProcessTypeBackground isEqualToString:type]) {
        return 0;
    }
    if ([LCProcessTypeAdaptive isEqualToString:type]) {
        return 2;
    }
    if ([LCProcessTypeInteractive isEqualToString:type]) {
        return 3;
    }

    return 1;   // standard value is LCProcessTypeStandard
}


- (void)setSelectedProcessTypeTag:(NSInteger)selectedTag
{
    switch (selectedTag) {
        case 0:
            [_selectedConfiguration setProcessType:LCProcessTypeBackground];
            break;
        case 1:
            [_selectedConfiguration setProcessType:LCProcessTypeStandard];
            break;
        case 2:
            [_selectedConfiguration setProcessType:LCProcessTypeAdaptive];
            break;
        case 3:
            [_selectedConfiguration setProcessType:LCProcessTypeInteractive];
            break;

        default:
            break;
    }
}


#pragma mark - Observer / Notifications


- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    if ([_sharedUserDefaultsController.defaults boolForKey:LCUserDefaultsSaveOnQuitFlagKey]) {
        // save all configurations
        [self saveAllModifiedConfigurations];
    } else if (self.modifiedConfigurationExists) {
        // TODO: Check any modified configurations and display an Alert, so the user can save all modifications when confirmed.
        NSAlert *alert = NSAlert.new;
        alert.alertStyle = NSAlertStyleWarning;
        id bundleName = [self.bundle objectForInfoDictionaryKey:@"CFBundleName"];
        alert.messageText = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"AlertMessageUnsavedConfigurations", nil, self.bundle,
                                                                                            @"Message text for an Alert informing about unsaved configurations."), bundleName];
        alert.informativeText = NSLocalizedStringFromTableInBundle(@"AlertTextUnsavedConfigurations", nil, self.bundle,
                                                                     @"Informative text for an Alert informing about unsaved configurations.");

        [alert addButtonWithTitle:NSLocalizedStringFromTableInBundle(@"LabelSavePanelDefaultButton", nil, self.bundle,
                                                                     @"Label of the default button for the save panel or the save alert.")];
        [alert addButtonWithTitle:NSLocalizedStringFromTableInBundle(@"LabelCancelButton", nil, self.bundle,
                                                                     @"Label of the Cancel button for the alerts and panels.")];

        if (NSAlertFirstButtonReturn == [alert runModal]) {
            [self saveAllModifiedConfigurations];
        }
        // Lines below doesn't work, because the main window is already closed!
        /*[alert beginSheetModalForWindow:self.mainView.window completionHandler:^(NSModalResponse returnCode) {
            if (NSModalResponseContinue == returnCode) {
                [self saveAllModifiedConfigurations];
            }
        }];*/
    }

    // save user defaults
    NSDictionary<LCUserDefaultsKey, id> *prefs = [_sharedUserDefaultsController.defaults persistentDomainForName:LCPrefPaneMainBundle.bundleIdentifier];

    // TODO: Fetch other actual settings from NSUserDefaultsController and store them in the NSUserDefaults

    [_sharedUserDefaultsController.defaults removePersistentDomainForName:LCPrefPaneMainBundle.bundleIdentifier];
    [_sharedUserDefaultsController.defaults setPersistentDomain:prefs forName:LCPrefPaneMainBundle.bundleIdentifier];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if ([@"objectValue" isEqualToString:keyPath]) {
        /* The URL of the path control has changed (by reading from plist or through UI), so check for nonexistent path components and color them red. */
        NSPathControl *pc = object;
        [pc.pathItems enumerateObjectsUsingBlock:^(NSPathControlItem * _Nonnull pci, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([NSFileManager.defaultManager fileExistsAtPath:pci.URL.path]) {
                return;
            }
            NSMutableDictionary<NSAttributedStringKey, id> *attributes = [NSMutableDictionary dictionaryWithDictionary:[pci.attributedTitle attributesAtIndex:0 effectiveRange:nil]];
            [attributes setObject:[NSNumber numberWithInteger:NSUnderlineStyleSingle] forKey:NSStrikethroughStyleAttributeName];
            [attributes setObject:NSColor.redColor forKey:NSForegroundColorAttributeName];
            pci.attributedTitle = [[NSAttributedString alloc] initWithString:pci.attributedTitle.string attributes:attributes];
        }];
    } else if (_sharedUserDefaultsController.defaults == object) {
            if([LCUserDefaultsShouldCheckFilesFlagKey isEqualToString:keyPath]) {
                // changing 'ShouldCheckFilesFlag', reload source list
                id newValue = [change objectForKey:NSKeyValueChangeNewKey];
                if (nil != newValue) {
                    [self refreshSourceList];
                }
            }
        // These observed keys are disabled, since there is no work to do (until now).
//            else if ([LCUserDefaultsSaveOnChangeFileFlagKey isEqualToString:keyPath]) {
//                /* nothing to do */
//            }
//            else if ([LCUserDefaultsSaveOnChangePrefPaneFlagKey isEqualToString:keyPath]) {
//                /* nothing to do */
//            }
//            else /* LCUserDefaultsSaveOnQuitFlagKey */ {
//                /* nothing to do */
//            }
    }
}


#pragma mark - Helper Methods / Functions


- (void)enumerateGroupItemsUsingBlock:(void (NS_NOESCAPE ^)(id group, NSInteger rowId, BOOL * _Nonnull stop))block {
    BOOL stop = NO;
    for (NSInteger rowNumber = 0; !stop && rowNumber < _sourceList.numberOfRows; rowNumber++) {
        id item = [_sourceList itemAtRow:rowNumber];
        if ([self outlineView:_sourceList isGroupItem:item]) {
            block(item, rowNumber, &stop);
        }
    }
}


- (void)enumerateJobConfigsUsingBlock:(void (NS_NOESCAPE ^)(LCJobConfig * _Nonnull config, BOOL * _Nonnull stop))block {
    [self enumerateGroupItemsUsingBlock:^(id groupItem, NSInteger rowId, BOOL * _Nonnull stopEnumeratingGroups) {
        NSArray<LCJobConfig *> *configurations = [groupItem objectForKey:LCConfigLocationConfigPathContentKey];
        [configurations enumerateObjectsUsingBlock:^(LCJobConfig * _Nonnull config, NSUInteger idx, BOOL * _Nonnull stopEnumeratingConfigs) {
            block(config, stopEnumeratingConfigs);
            *stopEnumeratingGroups = *stopEnumeratingConfigs;
        }];
    }];
}


- (BOOL)modifiedConfigurationExists
{
    __block BOOL modified = NO;
    [self enumerateJobConfigsUsingBlock:^(LCJobConfig * _Nonnull config, BOOL * _Nonnull stop) {
        modified = config.isModified;
        *stop = modified;
    }];
    return modified;
}


- (void)saveAllModifiedConfigurations
{
    [self enumerateJobConfigsUsingBlock:^(LCJobConfig * _Nonnull config, BOOL * _Nonnull stop) {
        [config save:nil];
    }];
}


- (void)refreshSourceList
{
    // save all expanded group items
    NSMutableIndexSet *expandedItemIndexes = NSMutableIndexSet.indexSet;
    const NSInteger groupItemCount = [_sourceList numberOfChildrenOfItem:nil];
    for (NSInteger i = 0; i < groupItemCount; i++) {
        id item = [_sourceList child:i ofItem:nil];
        assert(nil != item);
        if ([_sourceList isItemExpanded:item]) {
            [expandedItemIndexes addIndex:i];
        }
    }

    // invalidate cached data source
    [self.launchConfigLocations enumerateObjectsUsingBlock:^(LaunchConfiguration _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
        [item removeObjectForKey:LCConfigLocationConfigPathContentKey];
    }];
    [_sourceList reloadData];   // expanded child items will be closed after this call

    // restoring expanded group items
    [expandedItemIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
        id item = [self->_sourceList child:idx ofItem:nil];
        assert(nil != item);
        [self->_sourceList expandItem:item];
    }];
}


// Check for symbolic link and in that case determine and normalize/standardizing the destination path
static void resolveSymbolicLink(NSString **itemName)
{
    NSError *error = nil;

    NSDictionary<NSFileAttributeKey, id> *fileAttributes = [NSFileManager.defaultManager attributesOfItemAtPath:*itemName error:&error];
    if (nil != error) {
        NSLog(@"Error while fetching attributes of file %@:\n%@", *itemName, error);
    } else {
        if ([NSFileTypeSymbolicLink isEqualToString:[fileAttributes objectForKey:NSFileType]]) {
            NSString *resolvedItemName = [NSFileManager.defaultManager destinationOfSymbolicLinkAtPath:*itemName error:&error];
            if (nil != error) {
                NSLog(@"Error occured by resolving symbolic link:\n%@", error);
            } else {
                if ([resolvedItemName hasPrefix:@"../"]) {
                    *itemName = [[*itemName stringByDeletingLastPathComponent] stringByAppendingPathComponent:resolvedItemName];
                } else {
                    *itemName = resolvedItemName;
                }
                *itemName = [*itemName stringByStandardizingPath];
            }
        }
    }
}


- (nullable NSArray<LCJobConfig *> *)jobConfigurationsWithFilesOfDirectory:(NSString *)configPath error:(NSError **)error {
    NSDictionary<LCUserDefaultsKey, id> *defaults = [self.sharedUserDefaultsController.defaults persistentDomainForName:LCPrefPaneMainBundle.bundleIdentifier];
    BOOL shouldCheckFiles = [[defaults objectForKey:LCUserDefaultsShouldCheckFilesFlagKey] boolValue];    // TODO: make this flag configurable

    NSArray<NSString *> *dirContent = [NSFileManager.defaultManager contentsOfDirectoryAtPath:configPath error:error];
    if (nil == dirContent) {
        return nil;
    }

    // normalize all file names to a full qualified path and create lauch configuration objects
    LCJobConfig *(^mapper)(NSString * configFilename) = ^LCJobConfig *(NSString * configFilename) {
        NSString *checkedFilename = [configPath stringByAppendingPathComponent:configFilename];
        resolveSymbolicLink(&checkedFilename);
        return [LCJobConfig jobConfigWithContentsOfFile:checkedFilename];
    };

    // filter out all empty pathes (due to failure)
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(LCJobConfig *configuration, NSDictionary<NSString *, id> * _Nullable bindings) {
        if (nil != self->_searchString && 0 < self->_searchString.length) {
            if (![configuration.label localizedCaseInsensitiveContainsString:self->_searchString]) {
                return NO;
            }
        }
        if (shouldCheckFiles && ![NSFileManager.defaultManager fileExistsAtPath:configuration.filename]) {
            // This happens only for broken symbolic links.
            NSLog(@"Error fetching symbolic link to %@. File does not exists.", configuration.filename);
            return NO;
        }
        return YES;
    }];

    return [NSArray arrayWithArray:dirContent usingMapper:mapper filerUsingPredicate:predicate];
}


#pragma mark - action methods


- (IBAction)showProgramPathErrorAction:(id)sender
{
    NSDictionary<NSString *, NSString *> *errorUserInfo = self.selectedConfiguration.errorForProgramPath.userInfo;
    NSTextField *textFieldErrorDescription = [_errorPopover.contentViewController.view viewWithTag:1];
    textFieldErrorDescription.stringValue = [errorUserInfo objectForKey:NSLocalizedDescriptionKey];
    NSTextField *textFieldErrorSuggestion = [_errorPopover.contentViewController.view viewWithTag:2];
    textFieldErrorSuggestion.stringValue = [errorUserInfo objectForKey:NSLocalizedRecoverySuggestionErrorKey];
    [_errorPopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSRectEdgeMinX];
}


- (IBAction)showPreviewAction:(id)sender
{
    NSError *error = nil;

    // The launch configuration can exists as a binary plist file, so use the (de)serialization tools via NSData.
    NSPropertyListFormat format = 0;
    NSData *plistData = [NSData dataWithContentsOfFile:self.selectedConfiguration.filename];
    if (nil == plistData) {
        NSLog(@"No plist content for %@", self.selectedConfiguration.filename);
    } else {
        id plistSerial = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListImmutable format:&format error:&error];
        if (nil != error) {
            NSLog(@"Error occured by deseriallizing property list:\n%@", error);
        }
        if (nil != plistSerial && NSPropertyListBinaryFormat_v1_0 == format) {
            plistData = [NSPropertyListSerialization dataWithPropertyList:plistSerial format:NSPropertyListXMLFormat_v1_0 options:0 error:&error];
        }
    }

    NSString *plistFormat = nil;
    switch (format) {
        case NSPropertyListXMLFormat_v1_0:
            plistFormat = @"XML Format v1.0";
            break;
        case NSPropertyListBinaryFormat_v1_0:
            plistFormat = @"Binary Format v1.0";
            break;
        case NSPropertyListOpenStepFormat:
            plistFormat = @"OpenStep Format";
            break;

        default:
            break;
    }
    self.selectedFileFormat = plistFormat;

    if (nil != plistData) {
        // rebuild an xml structure from a dictionary structure
        NSString *xmlPlist = [[NSString alloc] initWithData:plistData encoding:NSUTF8StringEncoding];
        self.selectedFileContent = xmlPlist;
    } else {
        self.selectedFileContent = nil;
    }

    [_previewPopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSRectEdgeMinX];
}


- (IBAction)showInFinderAction:(id)sender
{
    NSURL *urlToShow = nil;
    NSString *rootFullPath = @"";

    if ([sender isMemberOfClass:NSPathControl.class]) {
        urlToShow = ((NSPathControl *)sender).clickedPathItem.URL;
        NSNumber *isDirectory = nil;
        BOOL success = [urlToShow getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:nil];
        if (success && isDirectory.boolValue) {
            /* if it's a directory, show its content */
            rootFullPath = urlToShow.URLByResolvingSymlinksInPath.path;
            urlToShow = nil;
        }
    } else {    /* here, sender should be a member of a NSMenuItem class */
        urlToShow = [NSURL fileURLWithPath:_selectedConfiguration.filename];
    }

    NSString *fullPath = urlToShow.URLByResolvingSymlinksInPath.path;
    BOOL wasSuccessfull = [NSWorkspace.sharedWorkspace selectFile:fullPath inFileViewerRootedAtPath:rootFullPath];
    if (!wasSuccessfull) {
        NSLog(@"File at %@ can not found for opening in Finder. (Perhaps this could be the destination path of an alias or symbolic link.)", fullPath);
    }
}


- (IBAction)startStopJob:(id)sender
{
    if (nil == _selectedConfiguration) {
        return;
    }

    BOOL successful = NO;
    NSError *error = nil;
    NSInteger pid = _selectedConfiguration.pid;
    if (0 != pid) {
        /* do stop this job */
        successful = [LCLaunchControl.sharedLaunchControl stopJob:_selectedConfiguration error:&error];
    } else {
        /* do start the selected job */
        successful = [LCLaunchControl.sharedLaunchControl startJob:_selectedConfiguration error:&error];
    }
    if (!successful) {
        NSLog(@"Error %@ job '%@'.\n%@", 0 != pid ? @"stopping" : @"starting", _selectedConfiguration.label, error);
        return;
    }

    NSString *pidSelectorName = NSStringFromSelector(@selector(pid));
    NSString *descriptionSelectorName = NSStringFromSelector(@selector(description));
    [_selectedConfiguration willChangeValueForKey:pidSelectorName];
    [_selectedConfiguration willChangeValueForKey:descriptionSelectorName];

    [_selectedConfiguration resetCachedValueForSelector:@selector(pid)];
    [_selectedConfiguration resetCachedValueForSelector:@selector(description)];

    [_selectedConfiguration didChangeValueForKey:descriptionSelectorName];
    [_selectedConfiguration didChangeValueForKey:pidSelectorName];
}


- (IBAction)loadUnloadJob:(id)sender
{
    if (nil == _selectedConfiguration) {
        return;
    }

    BOOL successful = NO;
    NSError *error = nil;
    BOOL isLoaded = _selectedConfiguration.isLoaded;
    if (isLoaded) {
        /* unload configuration from launchd */
        successful = [LCLaunchControl.sharedLaunchControl unloadJob:_selectedConfiguration error:&error];
    } else {
        /* load this configuration to lauchd */
        successful = [LCLaunchControl.sharedLaunchControl loadJob:_selectedConfiguration error:&error];
    }
    if (!successful) {
        NSLog(@"Error %@ job '%@'.\n%@", isLoaded ? @"unloading" : @"loading", _selectedConfiguration.label, error);
        return;
    }

    NSString *pidSelectorName = NSStringFromSelector(@selector(pid));
    NSString *isLoadedSelectorName = NSStringFromSelector(@selector(isLoaded));
    NSString *descriptionSelectorName = NSStringFromSelector(@selector(description));
    [_selectedConfiguration willChangeValueForKey:pidSelectorName];  // A load can cause automatic starting a job, so invalidate the PID
    [_selectedConfiguration willChangeValueForKey:isLoadedSelectorName];
    [_selectedConfiguration willChangeValueForKey:descriptionSelectorName];

    [_selectedConfiguration resetCachedValueForSelector:@selector(pid)];
    [_selectedConfiguration resetCachedValueForSelector:@selector(isLoaded)];
    [_selectedConfiguration resetCachedValueForSelector:@selector(description)];

    [_selectedConfiguration didChangeValueForKey:descriptionSelectorName];
    [_selectedConfiguration didChangeValueForKey:isLoadedSelectorName];
    [_selectedConfiguration didChangeValueForKey:pidSelectorName];
}


- (IBAction)showFindBar:(id)sender
{
    [_textFinder performAction:NSTextFinderActionShowFindInterface];
}


- (IBAction)searchJobByLabel:(NSSearchField *)sender
{
    _searchString = sender.stringValue;
    if (nil == _searchString) {
        return;
    }
    _searchString = [_searchString stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceCharacterSet];
    [self refreshSourceList];
}


- (IBAction)selectPathForExecutable:(NSPathControl *)sender
{
    NSOpenPanel *panel = NSOpenPanel.openPanel;
    panel.canChooseDirectories = NO;
    panel.treatsFilePackagesAsDirectories = YES;
    panel.canChooseFiles = YES;
    panel.allowsMultipleSelection = NO;
    panel.title = NSLocalizedStringFromTableInBundle(LCLabelOpenPanelSelectingExecuablePath, nil, LCPrefPaneMainBundle,
                                                     @"Open panel title for selecting the path to an executable.");
    panel.prompt = NSLocalizedStringFromTableInBundle(LCLabelOpenPanelDefaultButton, nil, LCPrefPaneMainBundle,
                                                      @"Label of the default button for the open panel selecting the path to an executable.");
    NSArray<NSPathControlItem *> *pis = sender.pathItems;
    NSUInteger indexOfFirstInvalidPathComponent = [pis indexOfObjectPassingTest:^BOOL(NSPathControlItem * _Nonnull pi, NSUInteger idx, BOOL * _Nonnull stop) {
        *stop = ![NSFileManager.defaultManager fileExistsAtPath:pi.URL.path];
        return *stop;
    }];
    if (NSNotFound == indexOfFirstInvalidPathComponent || 0 == indexOfFirstInvalidPathComponent) {
        panel.directoryURL = sender.URL;
    } else {
        // if there is an invalid path component found, set the current directory to that parent.
        NSPathControlItem *pc = pis[indexOfFirstInvalidPathComponent - 1];
        panel.directoryURL = pc.URL;
    }

    [panel beginSheetModalForWindow:self.mainView.window completionHandler:^(NSModalResponse result) {
        if (NSModalResponseOK != result) {
            return;
        }
        NSURL *selectedURL = panel.URLs.firstObject;
        if ([selectedURL isEqualTo:self->_selectedConfiguration.programURL]) {
            return;
        }
        self->_selectedConfiguration.programURL = selectedURL;
        [self->_sourceList reloadDataForRowIndexes:[NSIndexSet indexSetWithIndex:self->_sourceList.selectedRow]
                                     columnIndexes:[NSIndexSet indexSetWithIndex:0]];
    }];
}


- (IBAction)endEditingTextField:(NSTextField *)sender
{
    NSInteger rowNumber = [_tableView rowForView:sender];
    if (0 > rowNumber) {
        return;
    }
    NSString *changedValue = [sender.stringValue stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceCharacterSet];
    if (0 == rowNumber) {
        _selectedConfiguration.programPath = changedValue;
    } else {
        NSString *isModifiedSelectorName = NSStringFromSelector(@selector(isModified));
        NSMutableArray<NSMutableString *> *testArray = [_selectedConfiguration.propertyList objectForKey:LCLaunchdPlistProgramArgumentsKey];
        if (![changedValue isEqualToString:testArray[rowNumber]]) {
            [_selectedConfiguration willChangeValueForKey:isModifiedSelectorName];
            testArray[rowNumber].string = changedValue;
            [_selectedConfiguration didChangeValueForKey:isModifiedSelectorName];
        }
    }
    [_sourceList reloadDataForRowIndexes:[NSIndexSet indexSetWithIndex:_sourceList.selectedRow]
                           columnIndexes:[NSIndexSet indexSetWithIndex:0]];
}


#pragma mark - Protocol implementation of NSOutlineViewDataSource

// An example how to implement this interface, read article "Basic Cocoa Source List" at:
//  http://www.knowstack.com/sourcelist-in-cocoa/

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
    // for the root element...
    if (nil == item) {
        // ...just return the number of groups
        return _launchConfigLocations.count;
    }

    // ...else return the number of job configurations of a specific group
    NSArray<LCJobConfig *> *configurations = [item objectForKey:LCConfigLocationConfigPathContentKey];
    // if the job configurations of this group is already loaded...
    if (nil != configurations) {
        // ... just return the number of job configuration of tha group
        return configurations.count;
    }

    // ...else load all job configurations for that group
    NSError *error = nil;
    NSString *configPath = [[item objectForKey:LCConfigLocationConfigPathKey] stringByExpandingTildeInPath];
    configurations = [self jobConfigurationsWithFilesOfDirectory:configPath error:&error];
    if (nil != error) {
        NSLog(@"Error reading directory '%@'. %@", configPath, error);
        return 0;
    }
    [item setObject:configurations forKey:LCConfigLocationConfigPathContentKey];

    return configurations.count;
}


- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
    return [item respondsToSelector:@selector(count)];
}


- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item
{
    // for the root element...
    if (nil == item) {
        // ... return the configuration location of the selected group
        LaunchConfiguration launchConfigLocation = _launchConfigLocations[index];
        return launchConfigLocation;
    }

    // ...else return all job configurations of the selected group
    NSArray<LCJobConfig *> *dirContent = [item objectForKey:LCConfigLocationConfigPathContentKey];
    return dirContent[index];
}


- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
    NSString *value = nil;
    if ([self outlineView:outlineView isItemExpandable:item]) {
        value = [item objectForKey:LCConfigLocationGroupNameKey];
    } else {
        value = item;
    }
    return value;
}


- (void)outlineView:(NSOutlineView *)outlineView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
    // This method needs to be implemented if the SourceList is editable. e.g Changing the name of a Playlist in iTunes
    //[item setTitle:object];
}


#pragma mark - Protocol implementation of NSOutlineViewDelegate


- (BOOL)outlineView:(NSOutlineView *)outlineView shouldEditTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
    //Making the Source List Items Non Editable
    return NO;
}


- (NSView *)outlineView:(NSOutlineView *)outlineView viewForTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
    if (nil == item) {
        return nil;
    }

    NSTableCellView *view = nil;
    if ([self outlineView:outlineView isGroupItem:item]) {
        // Create the header row
        view = [outlineView makeViewWithIdentifier:@"HeaderCell" owner:self];
        NSString *groupName = [[item objectForKey:LCConfigLocationGroupNameKey] uppercaseString];
        view.textField.stringValue = groupName;

        NSImageName imageName = [item objectForKey:LCConfigLocationGroupIconNameKey];
        if (nil == imageName) {
            imageName = NSImageNameActionTemplate;
        }
        view.imageView.image = [NSImage imageNamed:imageName];

        [[view viewWithTag:1] setTitle:[[NSNumber numberWithUnsignedInteger:[self outlineView:outlineView numberOfChildrenOfItem:item]] description]];
    } else {
        NSAssert([item isKindOfClass:LCJobConfig.class],
                 @"Incorrect type of item, for that a column view should be created. Type is %@, but should be %@", [item className], LCJobConfig.className);
        LCJobConfig *launchConfig = item;

        // Create the data row
        view = [outlineView makeViewWithIdentifier:@"DataCell" owner:self];
        view.textField.stringValue = launchConfig.filename.lastPathComponent;

        BOOL isDir = NO;
        if (![NSFileManager.defaultManager fileExistsAtPath:launchConfig.filename isDirectory:&isDir] || isDir) {
            view.textField.textColor = NSColor.systemGrayColor;
            view.imageView.enabled = NO;
            view.imageView.image = nil;
        } else {
            if (!launchConfig.isConfigurationValid) {
                view.textField.textColor = NSColor.systemRedColor;
                view.imageView.enabled = NO;
            } else {
                /* Column views can be reused, so ensure resetting default values here. */
                view.textField.textColor = NSColor.textColor;
                view.imageView.enabled = YES;
            }
            view.imageView.image = launchConfig.programIcon;
        }
    }
    return view;
}


- (BOOL)outlineView:(NSOutlineView *)outlineView isGroupItem:(id)item
{
    //LaunchConfigurationList launchConfigLocations = [self launchConfigLocations];
    BOOL containsItem = (nil != item) &&
                        [item respondsToSelector:@selector(objectForKey:)] &&
                        ([item objectForKey:LCConfigLocationGroupNameKey] != nil);//[launchConfigLocations containsObject:item];
    return containsItem;
}


- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item
{
    // Here we are restricting users for selecting the Header/ Groups. Only the Data Cell Items can be selected. The group headers can only be shown or hidden.
    BOOL doSelect = nil != [outlineView parentForItem:item];
    return doSelect;
}


- (void)outlineViewSelectionDidChange:(NSNotification *)notification
{
    if ([_sharedUserDefaultsController.defaults boolForKey:LCUserDefaultsSaveOnChangeFileFlagKey]) {
        // save current configuration before switching or deselecting
        [self.selectedConfiguration save:nil];
    }

    NSOutlineView *ov = notification.object;
    NSIndexSet *selectedIndexes = ov.selectedRowIndexes;
    if (1 == selectedIndexes.count) {
        LCJobConfig *launchConfig = [ov itemAtRow:ov.selectedRow];
        BOOL isDir = NO;
        if (![NSFileManager.defaultManager fileExistsAtPath:launchConfig.filename isDirectory:&isDir] || isDir) {
            self.selectedConfiguration = nil;
        } else {
            self.selectedConfiguration = launchConfig;
        }

        //NSLog(@"Selected configuration:\n%@", _selectedConfiguration.propertyList.description);
    } else {
        // This is required only when multi-select (or nothing) is enabled in the SourceList/ Outline View and we are allowing users to do an action on multiple items
        self.selectedConfiguration = nil;
    }
}

@end
