//
//  LCConstants.h
//  LaunchCenterPrefPane
//
//  Created by henrik on 01.09.23.
//  Copyright © 2023 hackmac. All rights reserved.
//

#ifndef LCConstants_h
#define LCConstants_h


/// A global resource bundle variable for the main bundle of the Launch Center PrefPane makes it easy to access resources like localization strings
FOUNDATION_EXTERN NSBundle *LCPrefPaneMainBundle;


/* Keys for using with User Defaults */
typedef NSString *LCUserDefaultsKey NS_STRING_ENUM;

static LCUserDefaultsKey const LCUserDefaultsLaunchConfigLocationsKey = @"LaunchConfigLocations";
static LCUserDefaultsKey const LCUserDefaultsShouldCheckFilesFlagKey = @"ShouldCheckFilesFlag";
static LCUserDefaultsKey const LCUserDefaultsSaveOnChangeFileFlagKey = @"SaveOnChangeFileFlag";
static LCUserDefaultsKey const LCUserDefaultsSaveOnChangePrefPaneFlagKey = @"SaveOnChangePrefPaneFlag";
static LCUserDefaultsKey const LCUserDefaultsSaveOnQuitFlagKey = @"SaveOnQuitFlag";


/* Configuration keys using in the factory defaults plist file. A subset of LCUserDefaultsLaunchConfigLocationsKey */
typedef NSString *LCConfigLocationKey NS_STRING_ENUM;

static LCConfigLocationKey const LCConfigLocationConfigPathKey = @"ConfigPath"; // NSString
static LCConfigLocationKey const LCConfigLocationGroupNameKey = @"GroupName";   // NSString
static LCConfigLocationKey const LCConfigLocationGroupIconNameKey = @"GroupIconName";   // NSString

/* Configuration keys using for generated properties. */
static LCConfigLocationKey const LCConfigLocationConfigPathContentKey = @"ConfigPathContent";   // NSArray of LCJobConfig


/* Localization messages keys */
typedef NSString *LCLocalizationKey NS_STRING_ENUM;

static LCLocalizationKey const LCLabelOpenPanelSelectingExecuablePath = @"LabelOpenPanelSelectingExecuablePath";
static LCLocalizationKey const LCLabelOpenPanelDefaultButton = @"LabelOpenPanelDefaultButton";

static LCLocalizationKey const LCLaunchdErrorMsgInvalidConfigPath = @"ErrorMsgInvalidConfigPath";
static LCLocalizationKey const LCLaunchdErrorMsgProgramPathInvalid = @"ErrorMsgProgramPathInvalid";
static LCLocalizationKey const LCLaunchdRecoverSuggestProgramPathInvalid = @"RecoverSuggestProgramPathInvalid";
static LCLocalizationKey const LCLaunchdErrorMsgEmptyLabel = @"ErrorMsgEmptyLabel";
static LCLocalizationKey const LCLaunchdRecoverSuggestEmptyLabel = @"RecoverSuggestEmptyLabel";
static LCLocalizationKey const LCLaunchdErrorMsgMissingPlistFile = @"ErrorMsgMissingPlistFile";
static LCLocalizationKey const LCLaunchdRecoverSuggestMissingPlistFile = @"RecoverSuggestMissingPlistFile";
static LCLocalizationKey const LCLaunchdErrorMsgNoValidPlistFile = @"ErrorMsgNoValidPlistFile";

static LCLocalizationKey const LCLaunchdFormatDescription = @"FormatDescription";
static LCLocalizationKey const LCLaunchdFormatDescriptionWithPID = @"FormatDescriptionWithPID";
static LCLocalizationKey const LCLaunchdFormatDescriptionWithErrorMsg = @"FormatDescriptionWithErrorMsg";

static LCLocalizationKey const LCTooltipMessageUnloadStatus = @"TooltipMessageUnloadStatus";
static LCLocalizationKey const LCTooltipMessageLoadStatus = @"TooltipMessageLoadStatus";
static LCLocalizationKey const LCTooltipMessageStartStatus = @"TooltipMessageStartStatus";
static LCLocalizationKey const LCTooltipMessageStopStatus = @"TooltipMessageStopStatus";

#endif /* LCConstants_h */
