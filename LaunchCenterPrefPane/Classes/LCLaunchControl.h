//
//  LCLaunchControl.h
//  LaunchCenterPrefPane
//
//  Created by henrik on 06.08.18.
//  Copyright © 2018 hackmac. All rights reserved.
//

#import <Foundation/Foundation.h>


@class LCJobConfig;


NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSUInteger, LCLaunchControlJobStatus) {
    LCLaunchControlJobStatusUnknown = 1,    /* if this is set, other flags are ignored */
    LCLaunchControlJobStatusLoaded = 2,
    LCLaunchControlJobStatusStarted = 4
};


typedef NSString *LCCommandName NS_STRING_ENUM;

static LCCommandName const LCListCommand = @"list";
static LCCommandName const LCStopCommand = @"stop";
static LCCommandName const LCStartCommand = @"start";
static LCCommandName const LCErrorCommand = @"error";
static LCCommandName const LCEnableCommand = @"enable";
static LCCommandName const LCBootoutCommand = @"bootout";
static LCCommandName const LCBootstrapCommand = @"bootstrap";


/**
 Controller class that acts like a wrapper around `/bin/launchctl` and provides functions to its subcommands.
 */
@interface LCLaunchControl : NSObject

+ (instancetype)sharedLaunchControl;

- (LCLaunchControlJobStatus)jobStatus:(LCJobConfig *)jobConfig;

/// Returns a list with all loaded jobs, an array with three elements: PID, last exitcode, label
- (nullable NSArray<NSArray<NSString *> *> *)listLoadedJobs;
/// Typically returns a dictionary or `nil` if there is no job with this label loaded.
- (nullable id)listJobWithLabel:(nullable NSString *)label;

- (BOOL)startJob:(LCJobConfig *)jobConfig error:(out NSError * _Nullable *)error;
- (BOOL)stopJob:(LCJobConfig *)jobConfig error:(out NSError * _Nullable *)error;

- (BOOL)loadJob:(LCJobConfig *)jobBonfig error:(out NSError * _Nullable *)error;
- (BOOL)unloadJob:(LCJobConfig *)jobConfig error:(out NSError * _Nullable *)error;

/// Translates the given `errorCode`,  usually a return code from `launchctl`, into a locallized description returned as a new NSError object.
+ (NSError *)launchCtlErrorWithCode:(NSInteger)errorCode;

@end

NS_ASSUME_NONNULL_END
