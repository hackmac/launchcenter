//
//  LCJobConfig.m
//  LaunchCenterPrefPane
//
//  Created by henrik on 28.07.18.
//  Copyright © 2018 hackmac. All rights reserved.
//

#import <Appkit/AppKit.h>
#import "LCConstants.h"                     // defines and used for localization keys

#import "LCJobConfig.h"

#import "LCLaunchControl.h"

#import <sys/sysctl.h>


// Information about launchd Property List File can find on Apple documentation:
//  https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPSystemStartup/Chapters/CreatingLaunchdJobs.html#//apple_ref/doc/uid/10000172i-SW7-104142-BCICCCFI
// or by reading man launchd.plist(5)
// or another discussion of launchd http://technologeeks.com/docs/launchd.pdf

/* Keys for launchd Property List File, sorted alphabetically, status of 19 April, 2014 */
LCLaunchdPlistKey const LCLaunchdPlistAbandonProcessGroupKey = @"AbandonProcessGroup";      // boolean
LCLaunchdPlistKey const LCLaunchdPlistDebugKey = @"Debug";                                  // boolean
LCLaunchdPlistKey const LCLaunchdPlistDisabledKey = @"Disabled";                            // boolean or dictionary
LCLaunchdPlistKey const LCLaunchdPlistEnableGlobbingKey = @"EnableGlobbing";                // boolean
LCLaunchdPlistKey const LCLaunchdPlistEnablePressuredExitKey = @"EnablePressuredExit";      // boolean
LCLaunchdPlistKey const LCLaunchdPlistEnableTransactionsKey = @"EnableTransactions";        // boolean
LCLaunchdPlistKey const LCLaunchdPlistEnvironmentVariablesKey = @"EnvironmentVariables";    // dictionary of string
LCLaunchdPlistKey const LCLaunchdPlistExitTimeOutKey = @"ExitTimeOut";                      // integer
LCLaunchdPlistKey const LCLaunchdPlistGroupNameKey = @"GroupName";                          // string
LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsKey = @"HardResourceLimits";        // dictionary of integer
LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsCoreKey = @"Core";                      // integer
LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsCPUKey = @"CPU";                        // integer
LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsDataKey = @"Data";                      // integer
LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsFileSizeKey = @"FileSize";              // integer
LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsMemoryLockKey = @"MemoryLock";          // integer
LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsNumberOfFilesKey = @"NumberOfFiles";    // integer
LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsNumberOfProcessesKey = @"NumberOfProcesses"; // integer
LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsResidentSetSizeKey = @"ResidentSetSize"; // integer
LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsStackKey = @"Stack";                    // integer
LCLaunchdPlistKey const LCLaunchdPlistHopefullyExitsFirstKey = @"HopefullyExitsFirst";      // string
LCLaunchdPlistKey const LCLaunchdPlistHopefullyExitsLastKey = @"HopefullyExitsLast";        // string
LCLaunchdPlistKey const LCLaunchdPlistInetdCompatibilityKey = @"inetdCompatibility";        // dictionary
LCLaunchdPlistKey const LCLaunchdPlistInitGroupsKey = @"InitGroups";                        // boolean
LCLaunchdPlistKey const LCLaunchdPlistKeepAliveKey = @"KeepAlive";                          // boolean or dictionary of stuff
LCLaunchdPlistKey const LCLaunchdPlistKeepAliveCrashedKey = @"Crashed";                         // boolean
LCLaunchdPlistKey const LCLaunchdPlistKeepAliveNetworkStateKey = @"NetworkState";               // boolean, no longer implemented
LCLaunchdPlistKey const LCLaunchdPlistKeepAliveOtherJobEnabledKey = @"OtherJobEnabled";         // dictionary of boolean
LCLaunchdPlistKey const LCLaunchdPlistKeepAlivePathStateKey = @"PathState";                     // dictionary of boolean
LCLaunchdPlistKey const LCLaunchdPlistKeepAliveSuccessfulExitKey = @"SuccessfulExit";           // boolean
LCLaunchdPlistKey const LCLaunchdPlistLabelKey = @"Label";                                  // string, required
LCLaunchdPlistKey const LCLaunchdPlistLaunchEventsKey = @"LaunchEvents";                    // dictionary of dictionaries of dictionaries
LCLaunchdPlistKey const LCLaunchdPlistLaunchOnlyOnceKey = @"LaunchOnlyOnce";                // boolean
LCLaunchdPlistKey const LCLaunchdPlistLegacyTimersKey = @"LegacyTimers";                    // boolean
LCLaunchdPlistKey const LCLaunchdPlistLimitLoadFromHostsKey = @"LimitLoadFromHosts";        // array of string
LCLaunchdPlistKey const LCLaunchdPlistLimitLoadToHardwareKey = @"LimitLoadToHardware";      // dictionary of arrays
LCLaunchdPlistKey const LCLaunchdPlistLimitLoadToHostsKey = @"LimitLoadToHosts";            // array of string
LCLaunchdPlistKey const LCLaunchdPlistLimitLoadToSessionTypeKey = @"LimitLoadToSessionType";// string
LCLaunchdPlistKey const LCLaunchdPlistLowPriorityIOKey = @"LowPriorityIO";                  // boolean
LCLaunchdPlistKey const LCLaunchdPlistLowPriorityBackgroundIOKey = @"LowPriorityBackgroundIO"; // boolean
LCLaunchdPlistKey const LCLaunchdPlistMachServicesKey = @"MachServices";                    // dictionary of booleans or a dictionary of dictionaries
LCLaunchdPlistKey const LCLaunchdPlistMachServicesHideUntilCheckInKey = @"HideUntilCheckIn";    // boolean
LCLaunchdPlistKey const LCLaunchdPlistMachServicesResetAtCloseKey = @"ResetAtClose";            // boolean
LCLaunchdPlistKey const LCLaunchdPlistNiceKey = @"Nice";                                    // integer
LCLaunchdPlistKey const LCLaunchdPlistOnDemandKey = @"OnDemand";                            // boolean, deprecated and replaced by KeepAlive
LCLaunchdPlistKey const LCLaunchdPlistProcessTypeKey = @"ProcessType";                      // string, allowed types: "Adaptive", "Background", "Interactive", "Standard"
LCLaunchdPlistKey const LCLaunchdPlistProgramKey = @"Program";                              // string
LCLaunchdPlistKey const LCLaunchdPlistProgramArgumentsKey = @"ProgramArguments";            // array of string, required
LCLaunchdPlistKey const LCLaunchdPlistQueueDirectoriesKey = @"QueueDirectories";            // array of string
LCLaunchdPlistKey const LCLaunchdPlistRootDirectoryKey = @"RootDirectory";                  // string
LCLaunchdPlistKey const LCLaunchdPlistRunAtLoadKey = @"RunAtLoad";                          // boolean
LCLaunchdPlistKey const LCLaunchdPlistServiceIPCKey = @"ServiceIPC";                        // boolean
LCLaunchdPlistKey const LCLaunchdPlistSessionCreateKey = @"SessionCreate";                  // boolean
LCLaunchdPlistKey const LCLaunchdPlistSocketsKey = @"Sockets";                              // dictionary of dictionaries... OR dictionary of array of dictionaries
LCLaunchdPlistKey const LCLaunchdPlistSocketsBonjourKey = @"Bonjour";                           // boolean or string or array of strings
LCLaunchdPlistKey const LCLaunchdPlistSocketsMulticastGroupKey = @"MulticastGroup";             // string
LCLaunchdPlistKey const LCLaunchdPlistSocketsSecureSocketWithKeyKey = @"SecureSocketWithKey";   // string
LCLaunchdPlistKey const LCLaunchdPlistSocketsSockFamilyKey = @"SockFamily";                     // string, "IPv4", "IPv6" or "Unix"
LCLaunchdPlistKey const LCLaunchdPlistSocketsSockNodeNameKey = @"NodeName";                     // string
LCLaunchdPlistKey const LCLaunchdPlistSocketsSockPassiveKey = @"SockPassive";                   // boolean
LCLaunchdPlistKey const LCLaunchdPlistSocketsSockPathModeKey = @"SockPathMode";                 // integer
LCLaunchdPlistKey const LCLaunchdPlistSocketsSockPathName = @"SockPathName";                    // string, implies SockFamily is set to "Unix"
LCLaunchdPlistKey const LCLaunchdPlistSocketsSockPathOwnerKey = @"SockPathOwner";               // integer
LCLaunchdPlistKey const LCLaunchdPlistSocketsSockPathGroupKey = @"SockPathGroup";               // integer
LCLaunchdPlistKey const LCLaunchdPlistSocketsSockProtocolKey = @"SockProtocol";                 // string, at the moment are "TCP" and "UDP"
LCLaunchdPlistKey const LCLaunchdPlistSocketsSockServiceNameKey = @"SockServiceName";           // string
LCLaunchdPlistKey const LCLaunchdPlistSocketsSockTypeKey = @"SockType";                         // string, default is "stream", other valid values are "dgram" and "seqpacket"
LCLaunchdPlistKey const LCLaunchdPlistSoftResourceLimitsKey = @"SoftResourceLimits";        // dictionary of integer
LCLaunchdPlistKey const LCLaunchdPlistStandardErrorPathKey = @"StandardErrorPath";          // string
LCLaunchdPlistKey const LCLaunchdPlistStandardInPathKey = @"StandardInPath";                // string
LCLaunchdPlistKey const LCLaunchdPlistStandardOutPathKey = @"StandardOutPath";              // string
LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalKey = @"StartCalendarInterval";  // dictionary of integer or array of dictionary of integer
LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalDayKey = @"Day";                     // integer
LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalHourKey = @"Hour";                   // integer
LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalMinuteKey = @"Minute";               // integer
LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalMonthKey = @"Month";                 // integer
LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalWeekdayKey = @"Weekday";             // integer
LCLaunchdPlistKey const LCLaunchdPlistStartIntervalKey = @"StartInterval";                  // integer
LCLaunchdPlistKey const LCLaunchdPlistStartOnMountKey = @"StartOnMount";                    // boolean
LCLaunchdPlistKey const LCLaunchdPlistThrottleIntervalKey = @"ThrottleInterval";            // integer
LCLaunchdPlistKey const LCLaunchdPlistTimeOutKey = @"TimeOut";                              // integer
LCLaunchdPlistKey const LCLaunchdPlistUmaskKey = @"Umask";                                  // integer
LCLaunchdPlistKey const LCLaunchdPlistUserNameKey = @"UserName";                            // string
LCLaunchdPlistKey const LCLaunchdPlistWaitKey = @"Wait";                                    // boolean, inetdCompatibility related keys
LCLaunchdPlistKey const LCLaunchdPlistWaitForDebuggerKey = @"WaitForDebugger";              // boolean
LCLaunchdPlistKey const LCLaunchdPlistWatchPathsKey = @"WatchPaths";                        // array of string
LCLaunchdPlistKey const LCLaunchdPlistWorkingDirectoryKey = @"WorkingDirectory";            // string


// Definition of allowed types for the ProcessType key
LCProcessTypeValue const LCProcessTypeBackground = @"Background";
LCProcessTypeValue const LCProcessTypeStandard = @"Standard";
LCProcessTypeValue const LCProcessTypeAdaptive = @"Adaptive";
LCProcessTypeValue const LCProcessTypeInteractive = @"Interactive";


NSErrorDomain const LCLaunchdErrorDomain = @"LCLaunchdErrorDomain";


@interface LCJobConfig () {
    NSNumber *_pid;
    LCLaunchControlJobStatus _jobStatus;
    NSString *_description;
    NSImage *_appImage;
}

@property NSPropertyListFormat format;
/// This dictionary contains all informations that are stored in the origin plist file.
@property (readwrite, strong) NSMutableDictionary *propertyList;
/// Returns YES if the file of this configuration is only readable, otherwise NO.
@property (readwrite, getter=isReadOnly) BOOL readOnly;

@end


@implementation LCJobConfig

+ (nullable instancetype)jobConfigWithContentsOfFile:(NSString *)path
{
    NSError *error = nil;
    LCJobConfig *jobConfig = [self jobConfigWithContentsOfFile:path error:&error];
    if (nil != error) {
        NSLog(@"Error while reading job configuration:\n%@", error);
    }
    return jobConfig;
}


+ (nullable instancetype)jobConfigWithContentsOfFile:(NSString *)path error:(NSError *__autoreleasing  _Nullable * _Nullable)error
{
    BOOL isDir = NO;
    if (![NSFileManager.defaultManager fileExistsAtPath:path isDirectory:&isDir] || isDir) {
        NSString *errorMessage = NSLocalizedStringFromTableInBundle(LCLaunchdErrorMsgInvalidConfigPath, nil, LCPrefPaneMainBundle,
                                                                    @"Error message for a invalid path for a launchd configuration plist file.");
        NSUInteger errorCode = 1;

        NSDictionary<NSErrorUserInfoKey, id> *errorUserInfo = @{ NSLocalizedDescriptionKey: [NSString stringWithFormat:errorMessage, path] };
        if (nil != error) {
            *error = [NSError errorWithDomain:LCLaunchdErrorDomain code:errorCode userInfo:errorUserInfo];
        }
        return nil;
    }
    
    LCJobConfig *config = LCJobConfig.new;
    if (nil == config) {
        return nil;
    }

    config.filename = path;
    config.readOnly = ![NSFileManager.defaultManager isWritableFileAtPath:path];
    NSPropertyListFormat format = NSPropertyListXMLFormat_v1_0; // Custom creted configurations are always stred human readable.
    NSData *data = [NSData dataWithContentsOfFile:path options:0 error:error];
    config.propertyList = (nil == data)? nil : [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListMutableContainersAndLeaves format:&format error:error];
    config.format = format;
    return config;
}


+ (nullable instancetype)jobConfigWithPropertyList:(NSDictionary<NSString *, id> *)propertyList
{
    if (nil == propertyList) {
        return nil;
    }
    
    LCJobConfig *config = LCJobConfig.new;
    if (nil == config) {
        return nil;
    }

    config.filename = nil;
    config.readOnly = NO;
    config.propertyList = propertyList.mutableCopy;
    config.format = NSPropertyListXMLFormat_v1_0;
    return config;
}


- (instancetype)init
{
    self = [super init];
    if (nil == self) {
        return nil;
    }

    _pid = nil;
    _description = nil;
    _jobStatus = LCLaunchControlJobStatusUnknown;
    _appImage = nil;
    _propertyList = NSMutableDictionary.dictionary;

    return self;
}


#pragma mark -


- (BOOL)save:(out NSError *__autoreleasing _Nullable * _Nullable)error
{
    // Check if configuration is modified if there is an original given by filename
    if (!self.isModified) {
        return YES;
    }
    NSData *propList = [NSPropertyListSerialization dataWithPropertyList:_propertyList format:_format options:0 error:error];
    NSString *isModifiedSelectorName = NSStringFromSelector(@selector(isModified));
    [self willChangeValueForKey:isModifiedSelectorName];
    BOOL success = (nil != propList) && (nil == error || nil == *error) && [propList writeToFile:_filename options:NSDataWritingAtomic error:error];
    [self didChangeValueForKey:isModifiedSelectorName];
    return success;
}


+ (NSSet<NSString *> *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
    if ([NSStringFromSelector(@selector(isModified)) isEqualToString:key]) {
        NSString *propertyListSelectorName = NSStringFromSelector(@selector(propertyList));
        NSString *propertyListKeyPrefix = [propertyListSelectorName stringByAppendingString:@"."];
        return [NSSet setWithObjects:
                [propertyListKeyPrefix stringByAppendingString:LCLaunchdPlistProgramKey],
                //[propertyListKeyPrefix stringByAppendingString:LCLaunchdPlistLabelKey],   /* the value of the label is generally read only, and set once by creating a new configuration */
                [propertyListKeyPrefix stringByAppendingString:LCLaunchdPlistProgramArgumentsKey],
                [propertyListKeyPrefix stringByAppendingString:LCLaunchdPlistProcessTypeKey],
                [propertyListKeyPrefix stringByAppendingString:LCLaunchdPlistDisabledKey],
                [propertyListKeyPrefix stringByAppendingString:LCLaunchdPlistRunAtLoadKey],
                [propertyListKeyPrefix stringByAppendingString:LCLaunchdPlistStartOnMountKey],
                [propertyListKeyPrefix stringByAppendingString:LCLaunchdPlistKeepAliveKey],
                [propertyListKeyPrefix stringByAppendingString:LCLaunchdPlistAbandonProcessGroupKey],
                nil];
    } else if ([NSStringFromSelector(@selector(isProgramPathValid)) isEqualToString:key]) {
        return [NSSet setWithArray:@[
            NSStringFromSelector(@selector(programPath)),
            NSStringFromSelector(@selector(programURL))
        ]];
    }
    return [super keyPathsForValuesAffectingValueForKey:key];
}


- (BOOL)isModified
{
    if (nil == _filename || 0 >= _filename.length) {
        // there is no filename, so this must be a new configuration which have to be saved
        return YES;
    }

    LCJobConfig *originalConfig = [LCJobConfig jobConfigWithContentsOfFile:_filename];
    return ![self isEqualToLaunchdConfiguration:originalConfig] && (self.format == originalConfig.format);
}


- (BOOL)isEqualToLaunchdConfiguration:(LCJobConfig *)other
{
    if (nil == other || ![other isKindOfClass:self.class]) {
        return NO;
    }
    return [_filename isEqualToString:other.filename] && [_propertyList isEqualToDictionary:other.propertyList];
}


- (void)resetCachedValueForSelector:(SEL)selector
{
    if (@selector(pid) == selector) {
        if (nil != _pid) {
            _pid = nil;
        }
    } else if (@selector(description) == selector) {
        if (nil != _description) {
            _description = nil;
        }
    } else if (@selector(isLoaded) == selector) {
        if (LCLaunchControlJobStatusUnknown != _jobStatus) {
            _jobStatus = LCLaunchControlJobStatusUnknown;
        }
    } else if (@selector(programIcon) == selector) {
        if (nil != _appImage) {
            _appImage = nil;
        }
    } else {
        NSLog(@"No cache configured for selector %@", NSStringFromSelector(selector));
    }
}


- (NSInteger)pid
{
    return [self pid:nil];
}


- (pid_t)pid:(out NSError * _Nullable __autoreleasing *)retError
{
    if (nil != _pid) {
        return _pid.intValue;
    }

    _pid = @0;  // default value for case of an error
    NSString *label = self.label;
    if (nil != label)
#if MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_10
  #if false
    {
        // TODO: Implement a version which uses NSXPCConnection or the xpc_connection_* tools.
        xpc_connection_t xpcConn = xpc_connection_create_mach_service([label cStringUsingEncoding:NSUTF8StringEncoding], NULL, XPC_CONNECTION_MACH_SERVICE_PRIVILEGED);
        if (nil == xpcConn) {
            _pid = @0;
        } else {
            xpc_connection_set_event_handler(xpcConn, ^(xpc_object_t object) {
                xpc_type_t ty = xpc_get_type(object);
                if (ty == XPC_TYPE_CONNECTION) {
                    NSLog(@"%@", object);
                } else if (ty == XPC_TYPE_ERROR) {
                    NSLog(@"XPC error in server: %@", object);
                } else {
                    NSLog(@"unknown object received from XPC: %@", object);
                }
            });
            xpc_connection_resume(xpcConn);
            pid_t pid = xpc_connection_get_pid(xpcConn);
            // TODO: the returned PID is always zero!
            _pid = (0 == pid)? @0 : [NSNumber numberWithInt:pid];
        }
    }
  #else
    {
        // This implementation is using the commandline tool 'launchctl', which is really, really slow!
        _pid = [[LCLaunchControl.sharedLaunchControl listJobWithLabel:label] objectForKey:@"PID"];
    }
  #endif
#else
    {
        // This is an old fashioned implementation found in the sources of launchctl. Find a way to get those information using NSXPCConnection or the xpc_connection_* tools
        const char *c_label = [label cStringUsingEncoding:NSUTF8StringEncoding];

        launch_data_t msg = launch_data_alloc(LAUNCH_DATA_DICTIONARY);
        if (NULL != msg) {
            launch_data_dict_insert(msg, launch_data_new_string(c_label), LAUNCH_KEY_GETJOB);

            launch_data_t resp = launch_msg(msg);
            launch_data_free(msg);

            launch_data_t keyValue = launch_data_dict_lookup(resp, LAUNCH_JOBKEY_PID);
            if (NULL != keyValue) {
                launch_data_type_t t = launch_data_get_type(keyValue);
                if (LAUNCH_DATA_INTEGER == t) {
                    pid_t i = (pid_t)launch_data_get_integer(keyValue);
                    _pid = [NSNumber numberWithInt:i];
                    // NSLog(@"%s LOG: Found pid=%d for %@", __FUNCTION__, i, label);
                }
                launch_data_free(keyValue);
            } else {
                int errNo = launch_data_get_errno(resp);
                NSError *error = [LCLaunchControl launchCtlErrorWithCode:errNo];
                if (nil == retError) {
                    NSLog(@"%s ERROR: Faild getting pid for %@.\n%@", __FUNCTION__, label, error);
                } else {
                    *retError = error;
                }
            }

            launch_data_free(resp);
        }
    }
#endif
    return _pid.intValue;
}


- (NSImage *)runningStatusImage
{
    if (0 == self.pid) {
        return [NSImage imageNamed:NSImageNameGoRightTemplate];
    }
    return [NSImage imageNamed:NSImageNameStopProgressTemplate];
}


- (NSString *)runningStatusTooltipMessage
{
    if (0 == self.pid) {
        return NSLocalizedStringFromTableInBundle(LCTooltipMessageStartStatus, nil, LCPrefPaneMainBundle,
                                                  @"Tooltip message for menu items and buttons that informs that a job will be started when takes action.");
    }
    return NSLocalizedStringFromTableInBundle(LCTooltipMessageStopStatus, nil, LCPrefPaneMainBundle,
                                              @"Tooltip message for menu items and buttons that informs that a job will be stopped when takes action.");
}


- (BOOL)isLoaded
{
    if (LCLaunchControlJobStatusUnknown == _jobStatus) {
        // Cache this result! Because asking the commandline tool 'launchctl', which is really, really slow!
        _jobStatus = [LCLaunchControl.sharedLaunchControl jobStatus:self];
    }
    return (LCLaunchControlJobStatusLoaded & _jobStatus) != 0;
}


- (NSImage *)loadStatusImage
{
    if (self.isLoaded) {
        return [NSImage imageNamed:@"NSNavEjectButton.normal"];
    }
    return [NSImage imageNamed:NSImageNameRefreshTemplate];
}


- (NSString *)loadStatusTooltipMessage
{
    if (self.isLoaded) {
        return NSLocalizedStringFromTableInBundle(LCTooltipMessageUnloadStatus, nil, LCPrefPaneMainBundle,
                                                  @"Tooltip message for menu items and buttons that informs that a job configuration will be unload when takes action.");
    }
    return NSLocalizedStringFromTableInBundle(LCTooltipMessageLoadStatus, nil, LCPrefPaneMainBundle,
                                              @"Tooltip message for menu items and buttons that informs that a job configuration will be load when takes action.");
}


- (NSImage *)programIcon
{
    if (nil != _appImage) {
        return _appImage;
    }
    NSString *programPath = self.programPath;
    if (nil == programPath) {   // invalid configuration!
        return nil;
    }

    NSRange r = [programPath rangeOfString:@".app" options:NSCaseInsensitiveSearch];
    if (NSNotFound == r.location) {
        r = NSMakeRange(0, programPath.length);
    }
    _appImage = [NSWorkspace.sharedWorkspace iconForFile:[programPath substringToIndex:r.length + r.location]];

    return _appImage;
}


/*
 LaunchControl prioritizes the program arguments list.
 */
- (NSString *)programPath
{
    if (nil == _filename && nil == _propertyList) {
        return nil;
    }
    NSString *programPath = nil;
    NSNumber *c = [_propertyList valueForKeyPath:[LCLaunchdPlistProgramArgumentsKey stringByAppendingString:@".@count"]];
    if (0 < c.unsignedIntegerValue) {
        programPath = [_propertyList valueAtIndex:0 inPropertyWithKey:LCLaunchdPlistProgramArgumentsKey];
    }
    if (nil == programPath) {
        programPath = [_propertyList objectForKey:LCLaunchdPlistProgramKey];
    }

    return programPath;
}


/*
 LaunchControl prioritize to store the executable path into the program arguments list, so the user can edit it easily.
 */
- (void)setProgramPath:(NSString *)path
{
    NSString *propertyListSelectorName = NSStringFromSelector(@selector(propertyList));
    NSString *programURLSelectorName = NSStringFromSelector(@selector(programURL));
    NSString *programPathSelectorName = NSStringFromSelector(@selector(programPath));
    [self willChangeValueForKey:propertyListSelectorName];
    [self willChangeValueForKey:programURLSelectorName];
    [self willChangeValueForKey:programPathSelectorName];

    [_propertyList removeObjectForKey:LCLaunchdPlistProgramKey];  /* remove the Program value if set */

    NSNumber *c = [_propertyList valueForKeyPath:[LCLaunchdPlistProgramArgumentsKey stringByAppendingString:@".@count"]];
    if (0 == c.unsignedIntegerValue) {
        /* There is no Program Argument list until now... */
        [_propertyList insertValue:path atIndex:0 inPropertyWithKey:LCLaunchdPlistProgramArgumentsKey];
    } else {
        /* ...else replace the existing path entry at the first entry. */
        [_propertyList replaceValueAtIndex:0 inPropertyWithKey:LCLaunchdPlistProgramArgumentsKey withValue:path];
    }

    [self didChangeValueForKey:programPathSelectorName];
    [self didChangeValueForKey:programURLSelectorName];
    [self didChangeValueForKey:propertyListSelectorName];
}


- (NSURL *)programURL
{
    return (nil == self.programPath || 0 == self.programPath.length)? nil : [NSURL fileURLWithPath:self.programPath];
}


- (void)setProgramURL:(NSURL *)url
{
    self.programPath = url.path;
}


- (BOOL)isProgramPathValid
{
    BOOL isDir;
    NSString *programPath = self.programPath;
    return nil != programPath && [NSFileManager.defaultManager fileExistsAtPath:programPath isDirectory:&isDir] && !isDir;
}


- (BOOL)hasValidLabel
{
    if (nil == _filename && nil == _propertyList) {
        return NO;
    }
    NSString *label = self.label;
    return nil != label &&
        [label stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet].length > 0;
}


- (BOOL)isConfigurationValid
{
    return self.isProgramPathValid && self.hasValidLabel;
}


- (nullable NSError *)errorForProgramPath
{
    NSError *error = nil;

    if (!self.isProgramPathValid) {
        NSString *errorMessage = NSLocalizedStringFromTableInBundle(LCLaunchdErrorMsgProgramPathInvalid, nil, LCPrefPaneMainBundle,
                                                                    @"Error message for a invalid program path within a launchd configuration plist file.");
        NSString *recoverySuggestion = NSLocalizedStringFromTableInBundle(LCLaunchdRecoverSuggestProgramPathInvalid, nil, LCPrefPaneMainBundle,
                                                                          @"Recovery suggestion what to do when the program path is invalid.");
        NSUInteger errorCode = 2;

        NSDictionary<NSErrorUserInfoKey, id> *errorUserInfo = @{
                                                                NSLocalizedDescriptionKey: [NSString stringWithFormat:errorMessage, self.programPath.lastPathComponent],
                                                                NSLocalizedRecoverySuggestionErrorKey: recoverySuggestion
                                                                };
        error = [NSError errorWithDomain:LCLaunchdErrorDomain code:errorCode userInfo:errorUserInfo];
    }

    return error;
}


- (nullable NSError *)errorForLabel
{
    NSError *error = nil;

    if (!self.hasValidLabel) {
        NSString *errorMessage = NSLocalizedStringFromTableInBundle(LCLaunchdErrorMsgEmptyLabel, nil, LCPrefPaneMainBundle,
                                                                    @"Error message for a empty label value within a launchd configuration plist file.");
        NSString *recoverySuggestion = NSLocalizedStringFromTableInBundle(LCLaunchdRecoverSuggestEmptyLabel, nil, LCPrefPaneMainBundle,
                                                                          @"Recovery suggestion what to do, when the value of the label is wrong.");
        NSUInteger errorCode = 1;

        NSDictionary<NSErrorUserInfoKey, id> *errorUserInfo = @{
                                                                NSLocalizedDescriptionKey: errorMessage,
                                                                NSLocalizedRecoverySuggestionErrorKey: recoverySuggestion
                                                                };
        error = [NSError errorWithDomain:LCLaunchdErrorDomain code:errorCode userInfo:errorUserInfo];
    }

    return error;
}


- (NSString *)description
{
    if (nil != _description) {
        return _description;
    }

    BOOL isDir = NO;
    if (![NSFileManager.defaultManager fileExistsAtPath:_filename isDirectory:&isDir] || isDir) {
        NSString *descriptionFormat = NSLocalizedStringFromTableInBundle(LCLaunchdFormatDescriptionWithErrorMsg, nil, LCPrefPaneMainBundle,
                                                                         @"Format string to generate a description of a launch configuration which includes an error message.");
        NSString *errorMessage = NSLocalizedStringFromTableInBundle(LCLaunchdErrorMsgMissingPlistFile, nil, LCPrefPaneMainBundle,
                                                                    @"Error message for a invalid configuration path.");
        _description = [NSString stringWithFormat:descriptionFormat, _filename.lastPathComponent, errorMessage];
        return _description;
    }

    if (!self.hasValidLabel) {
        _description = NSLocalizedStringFromTableInBundle(LCLaunchdErrorMsgNoValidPlistFile, nil, LCPrefPaneMainBundle,
                                                          @"Error message for a invalid launchd configuration plist file.");
        return _description;
    }

    if (!self.isProgramPathValid) {
        NSString *descriptionFormat = NSLocalizedStringFromTableInBundle(LCLaunchdFormatDescriptionWithErrorMsg, nil, LCPrefPaneMainBundle,
                                                                         @"Format string to generate a description of a launch configuration which includes an error message.");
        NSString *errorMessage = [self.errorForProgramPath.userInfo objectForKey:NSLocalizedDescriptionKey];
        _description = [NSString stringWithFormat:descriptionFormat, self.programPath, errorMessage];
        return _description;
    }

    NSInteger pid = self.pid;
    if (0 == pid) {
        NSString *descriptionFormat = NSLocalizedStringFromTableInBundle(LCLaunchdFormatDescription, nil, LCPrefPaneMainBundle,
                                                                         @"Format string to generate a description of a launch configuration.");
        _description = [NSString stringWithFormat:descriptionFormat, self.label, _filename];
        return _description;
    }

    NSString *descriptionFormat = NSLocalizedStringFromTableInBundle(LCLaunchdFormatDescriptionWithPID, nil, LCPrefPaneMainBundle,
                                                                     @"Format string to generate a description of a launch configuration which includes the process ID.");
    _description = [NSString stringWithFormat:descriptionFormat, self.label, pid, _filename];
    return _description;
}


#pragma mark - convenience methods accessing property values


- (nullable NSString *)label
{
    NSString *label = [_propertyList objectForKey:LCLaunchdPlistLabelKey];
    return label;
}


- (void)setLabel:(nullable NSString *)label
{
    if (nil == label) {
        [_propertyList removeObjectForKey:LCLaunchdPlistLabelKey];
    } else {
        [_propertyList setObject:label.copy forKey:LCLaunchdPlistLabelKey];
    }
}


- (LCProcessTypeValue)processType
{
    LCProcessTypeValue type = [_propertyList objectForKey:LCLaunchdPlistProcessTypeKey];

    // 'Standart' by default
    return (nil != type)? type : LCProcessTypeStandard;
}


- (void)setProcessType:(LCProcessTypeValue)type
{
    if ([LCProcessTypeStandard isEqualToString:type]) {
        [_propertyList removeObjectForKey:LCLaunchdPlistProcessTypeKey];
    } else {
        [_propertyList setObject:type forKey:LCLaunchdPlistProcessTypeKey];
    }
}


- (BOOL)disabled
{
    static bool (^sysctl_hw_streq)(int, NSString *) = ^ bool (int mib_slot, NSString *str) {
        char buf[1000];
        size_t bufsz = sizeof(buf);
        int mib[] = { CTL_HW, mib_slot };

        if (sysctl(mib, 2, buf, &bufsz, NULL, 0) != -1) {
            if (strcmp(buf, [str cStringUsingEncoding:NSUTF8StringEncoding]) == 0) {
                return true;
            }
        }

        return false;
    };

    id objDisabled = [_propertyList objectForKey:LCLaunchdPlistDisabledKey];

    if (nil == objDisabled) {
        return NO;
    }

    if ([objDisabled isKindOfClass:NSNumber.class]) {
        return ((NSNumber *) objDisabled).boolValue;
    }

    if ([objDisabled isKindOfClass:NSDictionary.class]) {
        __block BOOL retVal = false;
        // if object for 'Disabled' key is a directory, check the keys 'ModelName' and 'MachineType'. See launchctl.c -> job_disabled_dict_logic()
        [((NSDictionary *) objDisabled) enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if (![obj isKindOfClass:NSString.class]) {
                return;
            }

            // TODO: There are some special cases, i.e. in /System/Library/LaunchDaemons/com.apple.containermanagerd.system.plist
            //  with secret keys: "#IfFeatureFlagDisabled" and "#Then"

            if ([key isEqualToString:@LAUNCH_JOBKEY_DISABLED_MACHINETYPE]) {
                if (sysctl_hw_streq(HW_MACHINE, obj)) {
                    retVal = true;
                }
            } else if ([key isEqualToString:@LAUNCH_JOBKEY_DISABLED_MODELNAME]) {
                if (sysctl_hw_streq(HW_MODEL, obj)) {
                    retVal = true;
                }
            }
        }];
        return retVal;
    }

    return NO;
}


- (void)setDisabled:(BOOL)disabled
{
    if (disabled) {
        [_propertyList setObject:@YES forKey:LCLaunchdPlistDisabledKey];
    } else {
        [_propertyList removeObjectForKey:LCLaunchdPlistDisabledKey];
    }
}


- (BOOL)runAtLoad
{
    NSNumber *value = [_propertyList objectForKey:LCLaunchdPlistRunAtLoadKey];

    // false by default
    return nil != value && value.boolValue;
}


- (void)setRunAtLoad:(BOOL)runAtLoad
{
    if (runAtLoad) {
        [_propertyList setObject:@YES forKey:LCLaunchdPlistRunAtLoadKey];
    } else {
        [_propertyList removeObjectForKey:LCLaunchdPlistRunAtLoadKey];
    }
}


- (BOOL)startOnMount
{
    NSNumber *value = [_propertyList objectForKey:LCLaunchdPlistStartOnMountKey];

    // false by default
    return nil != value && value.boolValue;
}


- (void)setStartOnMount:(BOOL)startOnMount
{
    if (startOnMount) {
        [_propertyList setObject:@YES forKey:LCLaunchdPlistStartOnMountKey];
    } else {
        [_propertyList removeObjectForKey:LCLaunchdPlistStartOnMountKey];
    }
}


- (BOOL)keepAlive
{
    NSNumber *value = [_propertyList objectForKey:LCLaunchdPlistKeepAliveKey];

    // default is false
    return nil != value && [value isKindOfClass:NSNumber.class] && value.boolValue;
}


- (void)setKeepAlive:(BOOL)keepAlive
{
    if (keepAlive) {
        [_propertyList setObject:@YES forKey:LCLaunchdPlistKeepAliveKey];
    } else {
        [_propertyList removeObjectForKey:LCLaunchdPlistKeepAliveKey];
    }
}


- (BOOL)onDemand
{
    NSNumber *value = [_propertyList objectForKey:LCLaunchdPlistOnDemandKey];

    // YES by default
    return nil == value || value.boolValue;
}


- (void)setOnDemand:(BOOL)onDemand
{
    if (onDemand) {
        [_propertyList removeObjectForKey:LCLaunchdPlistOnDemandKey];
    } else {
        [_propertyList setObject:@NO forKey:LCLaunchdPlistOnDemandKey];
    }
}


- (BOOL)abandonProcessGroup
{
    NSNumber *value = [_propertyList objectForKey:LCLaunchdPlistAbandonProcessGroupKey];

    // default is false
    return nil != value && value.boolValue;
}


- (void)setAbandonProcessGroup:(BOOL)abandonProcessGroup
{
    if (abandonProcessGroup) {
        [_propertyList setObject:@YES forKey:LCLaunchdPlistAbandonProcessGroupKey];
    } else {
        [_propertyList removeObjectForKey:LCLaunchdPlistAbandonProcessGroupKey];
    }
}

@end
