//
//  LCJobConfig.h
//  LaunchCenterPrefPane
//
//  Created by henrik on 28.07.18.
//  Copyright © 2018 hackmac. All rights reserved.
//

#import <Foundation/Foundation.h>


// Information about launchd Property List File can find on Apple documentation:
//  https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPSystemStartup/Chapters/CreatingLaunchdJobs.html#//apple_ref/doc/uid/10000172i-SW7-104142-BCICCCFI
// or by reading man launchd.plist(5)


NS_ASSUME_NONNULL_BEGIN

/* Keys for launchd Property List File, sorted alphabetically, status of 19 April, 2014 */
typedef NSString *LCLaunchdPlistKey NS_STRING_ENUM;

FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistAbandonProcessGroupKey;                 // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistDebugKey;                               // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistDisabledKey;                            // boolean or dictionary
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistEnableGlobbingKey;                      // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistEnablePressuredExitKey;                 // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistEnableTransactionsKey;                  // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistEnvironmentVariablesKey;                // dictionary of string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistExitTimeOutKey;                         // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistGroupNameKey;                           // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsKey;                  // dictionary of integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsCoreKey;                  // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsCPUKey;                   // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsDataKey;                  // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsFileSizeKey;              // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsMemoryLockKey;            // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsNumberOfFilesKey;         // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsNumberOfProcessesKey;     // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsResidentSetSizeKey;       // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHardResourceLimitsStackKey;                 // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHopefullyExitsFirstKey;                 // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistHopefullyExitsLastKey;                  // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistInetdCompatibilityKey;                  // dictionary
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistInitGroupsKey;                          // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistKeepAliveKey;                           // boolean or dictionary of stuff
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistKeepAliveCrashedKey;                        // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistKeepAliveNetworkStateKey;                   // boolean, no longer implemented
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistKeepAliveOtherJobEnabledKey;                // dictionary of boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistKeepAlivePathStateKey;                      // dictionary of boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistKeepAliveSuccessfulExitKey;                 // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistLabelKey;                               // string, required
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistLaunchEventsKey;                        // dictionary of dictionaries of dictionaries
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistLaunchOnlyOnceKey;                      // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistLegacyTimersKey;                        // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistLimitLoadFromHostsKey;                  // array of string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistLimitLoadToHardwareKey;                 // dictionary of arrays
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistLimitLoadToHostsKey;                    // array of string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistLimitLoadToSessionTypeKey;              // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistLowPriorityIOKey;                       // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistLowPriorityBackgroundIOKey;             // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistMachServicesKey;                        // dictionary of booleans or a dictionary of dictionaries
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistMachServicesHideUntilCheckInKey;            // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistMachServicesResetAtCloseKey;                // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistNiceKey;                                // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistOnDemandKey NS_DEPRECATED_WITH_REPLACEMENT_MAC("LCLaunchdPlistKeepAliveKey", 10_0, 10_5); // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistProcessTypeKey;                         // string, allowed types: "Adaptive", "Background", "Interactive", "Standard"
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistProgramKey;                             // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistProgramArgumentsKey;                    // array of string, required
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistQueueDirectoriesKey;                    // array of string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistRootDirectoryKey;                       // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistRunAtLoadKey;                           // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistServiceIPCKey NS_DEPRECATED_MAC(10_0, 10_5); // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSessionCreateKey;                       // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsKey;                             // dictionary of dictionaries... OR dictionary of array of dictionaries
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsBonjourKey;                          // boolean or string or array of strings
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsMulticastGroupKey;                   // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsSecureSocketWithKeyKey;              // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsSockFamilyKey;                       // string, "IPv4", "IPv6" or "Unix"
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsSockNodeNameKey;                     // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsSockPassiveKey;                      // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsSockPathModeKey;                     // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsSockPathName;                        // string, implies SockFamily is set to "Unix"
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsSockPathOwnerKey;                    // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsSockPathGroupKey;                    // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsSockProtocolKey;                     // string, at the moment are "TCP" and "UDP"
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsSockServiceNameKey;                  // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSocketsSockTypeKey;                         // string, default is "stream", other valid values are "dgram" and "seqpacket"
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistSoftResourceLimitsKey;                  // dictionary of integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistStandardErrorPathKey;                   // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistStandardInPathKey;                      // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistStandardOutPathKey;                     // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalKey;               // dictionary of integer or array of dictionary of integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalDayKey;                // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalHourKey;               // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalMinuteKey;             // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalMonthKey;              // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistStartCalendarIntervalWeekdayKey;            // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistStartIntervalKey;                       // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistStartOnMountKey;                        // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistThrottleIntervalKey;                    // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistTimeOutKey;                             // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistUmaskKey;                               // integer
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistUserNameKey;                            // string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistWaitKey;                                // boolean, inetdCompatibility related keys
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistWaitForDebuggerKey;                     // boolean
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistWatchPathsKey;                          // array of string
FOUNDATION_EXPORT LCLaunchdPlistKey const LCLaunchdPlistWorkingDirectoryKey;                    // string


// Definition of allowed types for the ProcessType key
typedef NSString *LCProcessTypeValue NS_STRING_ENUM;

FOUNDATION_EXPORT LCProcessTypeValue const LCProcessTypeBackground;
FOUNDATION_EXPORT LCProcessTypeValue const LCProcessTypeStandard;
FOUNDATION_EXPORT LCProcessTypeValue const LCProcessTypeAdaptive;
FOUNDATION_EXPORT LCProcessTypeValue const LCProcessTypeInteractive;



/**
 This class contains a complete job configuration that will be loaded by the `launchd` process.

 Objects of this class are used for data objects for the detail view of the preference pane.
 */
@interface LCJobConfig : NSObject

@property (nullable, copy) NSString *filename;
/// This dictionary contains all informations that are stored in the origin plist file.
@property (readonly, strong) NSMutableDictionary *propertyList;

+ (nullable instancetype)jobConfigWithPropertyList:(NSDictionary<NSString *, id> * _Nonnull)propertyList;
+ (nullable instancetype)jobConfigWithContentsOfFile:(NSString *)path error:(NSError *__autoreleasing  _Nullable * _Nullable)error;
+ (nullable instancetype)jobConfigWithContentsOfFile:(NSString *)path;

- (BOOL)save:(out NSError *__autoreleasing _Nullable * _Nullable)error;

/**
 Checks if any of the properties of the configuration was modified.

 Every time the method is called, it loads the content of the original file and calls `isEqualToLaunchdConfiguration:` to check equation.
 */
@property (readonly) BOOL isModified;
/// Returns YES if the file of this configuration is only readable, otherwise NO.
@property (readonly, getter=isReadOnly) BOOL readOnly;
- (BOOL)isEqualToLaunchdConfiguration:(LCJobConfig *)object;

- (void)resetCachedValueForSelector:(SEL)selector;

/// The process ID for the job of a launchd configuration, if running, `0` if the job is not running.
@property (readonly) NSInteger pid;
- (pid_t)pid:(out NSError * _Nullable __autoreleasing *)retError;

@property (readonly, copy) NSImage *runningStatusImage;
@property (readonly, copy) NSString *runningStatusTooltipMessage;

/// A string that represents the launchd job configuration file, usually used for UI tooltip. It contains information about PID, status, the program path and possible error messages.
@property (readonly, copy) NSString *description;

/**
 Checks if this configuration is loaded by the launchd process.

 This method caches its computed value. You have to call <code>resetCachedValueForSelector:@selector(isLoaded)</code> for resetting the cache.

 @returns
    `YES` if that configuration is loaded by lauchd, otherwise `NO`.
 */
@property (readonly) BOOL isLoaded;
@property (readonly, copy) NSImage *loadStatusImage;
@property (readonly, copy) NSString *loadStatusTooltipMessage;

/// Obtains the icon from the specified path value.
@property (nullable, readonly) NSImage *programIcon;

/**
 Contains the path from the value of `LCLaunchdPlistProgramKey` or from the first entry of `LCLaunchdPlistProgramArgumentsKey`.

 By setting this property, it affects the value of `programURL`!
 */
@property (nullable) NSString *programPath;
/**
 Contains the URL from the value of `LCLaunchdPlistProgramKey` or from the first entry of `LCLaunchdPlistProgramArgumentsKey`.

 By setting this property, it affects the value of `programPath`!
 */
@property (nullable) NSURL *programURL;

/// Set to `YES` if the path of the program exists in the file system.
@property (readonly) BOOL isProgramPathValid;
/// Set to `YES` if the label is set and not empty in the configuration, `NO` otherwise.
@property (readonly) BOOL hasValidLabel;
/// Set to `YES` if the whole configuration is valid, `NO` otherwise. This is archived by testing the path and other mandatory key values.
@property (readonly) BOOL isConfigurationValid;

@property (nullable, readonly) NSError *errorForProgramPath;
@property (nullable, readonly) NSError *errorForLabel;

#pragma mark - convenience methods accessing property values

@property (nullable, copy) NSString *label;

@property (copy) LCProcessTypeValue processType;

@property BOOL disabled;

@property BOOL runAtLoad;
@property BOOL startOnMount;

@property BOOL keepAlive;
@property BOOL onDemand NS_DEPRECATED_MAC(10_0, 10_5);

@property BOOL abandonProcessGroup;

@end

NS_ASSUME_NONNULL_END
