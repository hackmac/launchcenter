//
//  LCLaunchControl.m
//  LaunchCenterPrefPane
//
//  Created by henrik on 06.08.18.
//  Copyright © 2018 hackmac. All rights reserved.
//

#import "LCLaunchControl.h"

#import "HWArrayMapping.h"
#import "LCJobConfig.h"


static NSString *const LCLaunchControlPath = @"/bin/launchctl";


@interface LCLaunchControl ()

/**
 Starts a new `launchctl` process.
 @discussion
    Use this method (instead of <code>+launchCtl:arguments:returning:error:</code>) for commands that have no output and are executed silently.
 @param subcommand
    A value that specifies the command to start.
 @param args
    The list of arguments that the command need to run.
 @return
    The exit code of the process.
*/
+ (NSInteger)launchCtl:(LCCommandName)subcommand arguments:(NSArray<NSString *> * _Nullable)args;

/**
 Starts a new <i>launchctl</i> process, waits for termination and grabs the output of the process into a new data object that will be returned in <code>data</code>.
 @param subcommand
    One of the values defined as <code>LCCommandName</code>, that specifies the command to start.
 @param args
    Pass a list of arguments that the command needs to run.
 @param data
    A Pointer to a variable to store the returning data as a result of the <code>subcommand</code>. In case of an error, Its value is left unchanged.
 @param error
    An optional pointer to a variable that stores error information. It will be set to <code>nil</code> if no error occures, else it will contain a new <code>NSError</code> object.
 @return
    The exit code of the process.
*/
+ (NSInteger)launchCtl:(LCCommandName)subcommand
             arguments:(NSArray<NSString *> * _Nullable)args
             returning:(out NSData * _Nonnull __autoreleasing *)data
                 error:(out NSError * _Nullable __autoreleasing *)error;

- (id)parseLaunchCtrlOutput:(NSString *)outputString;
- (id)parseContainer:(NSScanner *)scanner;
- (NSDictionary<NSString *, id> *)parseDictionary:(NSScanner *)scanner;
- (NSArray<id> *)parseArray:(NSScanner *)scanner;
- (NSString *)parseConstant:(NSScanner *)scanner;
- (NSString *)parseString:(NSScanner *)scanner;
- (NSNumber *)parseInteger:(NSScanner *)scanner;
- (NSNumber *)parseBoolean:(NSScanner *)scanner;
- (NSString *)parseKey:(NSScanner *)scanner;

@end


@implementation LCLaunchControl

+ (instancetype)sharedLaunchControl
{
    static LCLaunchControl *lc;

#if MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_6
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        lc = [LCLaunchControl new];
    });
    return lc;
#else
    @synchronized(self) {
        if (nil == lc) {
            lc = [LCLaunchControl new];
        }

        return lc;
    }
#endif
}


- (LCLaunchControlJobStatus)jobStatus:(LCJobConfig *)jobConfig
{
    LCLaunchControlJobStatus retVal = 0;

    NSDictionary *jobInfo = [self listJobWithLabel:jobConfig.label];
    if (nil != jobInfo) {
        retVal |= LCLaunchControlJobStatusLoaded;
    }
    if (nil != [jobInfo objectForKey:@LAUNCH_JOBKEY_PID]) {
        retVal |= LCLaunchControlJobStatusStarted;
    }

    return retVal;
}


- (NSArray<NSArray<NSString *> *> *)listLoadedJobs
{
    NSError *error = nil;
    NSData *outputData = nil;
    const NSInteger status = [LCLaunchControl launchCtl:LCListCommand arguments:nil returning:&outputData error:&error];
    if (0 != status || nil == outputData) {
        NSLog(@"Error %@", [error.userInfo objectForKey:NSLocalizedDescriptionKey]);
        return nil;
    }

    NSString *outputString = [[NSString alloc] initWithData:outputData encoding:NSUTF8StringEncoding];
    NSLog(@"output string: %@", outputString);
    NSArray<NSString *> *tempArr = [outputString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    NSArray<NSArray<NSString *> *> *result = [tempArr mappedArrayUsingBlock:^NSArray<NSString *> *(NSString * _Nonnull obj) {
        NSArray<NSString *> *tempArray = [obj componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        return tempArray;
    }];
    NSLog(@"parsed result: %@", result);

    return result;
}


- (id)listJobWithLabel:(NSString *)label
{
    NSError *error = nil;
    NSData *outputData = nil;
    NSArray<NSString *> *cmdArgs = nil == label ? nil : @[label];
    const NSInteger status = [LCLaunchControl launchCtl:LCListCommand arguments:cmdArgs returning:&outputData error:&error];
    if (0 != status || nil == outputData) {
        NSLog(@"Error %@", [error.userInfo objectForKey:NSLocalizedDescriptionKey]);
        return nil;
    }

    NSString *outputString = [[NSString alloc] initWithData:outputData encoding:NSUTF8StringEncoding];
    id result = [self parseLaunchCtrlOutput:outputString];

    return result;
}


- (BOOL)startJob:(LCJobConfig *)config error:(out NSError *__autoreleasing _Nullable *)error
{
    NSString *label = config.label;
    NSArray<NSString *> *cmdArgs = nil == label ? nil : @[label];
    NSInteger status = [LCLaunchControl launchCtl:LCStartCommand arguments:cmdArgs];
    if (0 != status) {
        NSError *e = [LCLaunchControl launchCtlErrorWithCode:status];
        if (nil != error) {
            *error = e;
        } else {
            NSLog(@"Error: 'launchctl start %@' exits with code %ld: %@",
                  label, status, [e.userInfo objectForKey:NSLocalizedDescriptionKey]);
        }
        return NO;
    }

    return YES;
}


- (BOOL)stopJob:(LCJobConfig *)config error:(out NSError *__autoreleasing _Nullable *)error
{
    NSString *label = config.label;
    NSArray<NSString *> *cmdArgs = nil == label ? nil : @[label];
    NSInteger status = [LCLaunchControl launchCtl:LCStopCommand arguments:cmdArgs];
    if (0 != status) {
        NSError *e = [LCLaunchControl launchCtlErrorWithCode:status];
        if (nil != error) {
            *error = e;
        } else {
            NSLog(@"Error: 'launchctl stop %@' exits with code %ld: %@",
                  label, status, [e.userInfo objectForKey:NSLocalizedDescriptionKey]);
        }
        return NO;
    }

    return YES;
}


- (BOOL)loadJob:(LCJobConfig *)config error:(out NSError *__autoreleasing _Nullable *)errorPtr
{
    uid_t uid = getuid();
    NSString *domainTarget = [NSString stringWithFormat:@"gui/%d", uid];
    NSString *serviceTarget = [NSString stringWithFormat:@"gui/%d/%@", uid, config.label];
    NSString *plistFilePath = config.filename;

    NSInteger status = [LCLaunchControl launchCtl:LCBootstrapCommand arguments:@[domainTarget, plistFilePath]];
    if (0 != status) {
        NSError *error = [LCLaunchControl launchCtlErrorWithCode:status];
        if (nil != errorPtr) {
            *errorPtr = error;
        } else {
            NSLog(@"Error: 'launchctl bootstrap %@ %@' exits with code %ld: %@",
                  domainTarget, plistFilePath, status, [error.userInfo objectForKey:NSLocalizedDescriptionKey]);
        }
        return NO;
    }

    status = [LCLaunchControl launchCtl:LCEnableCommand arguments:@[serviceTarget, plistFilePath]];
    if (0 != status) {
        NSError *error = [LCLaunchControl launchCtlErrorWithCode:status];
        if (nil != errorPtr) {
            *errorPtr = error;
        } else {
            NSLog(@"Error: 'launchctl enable %@ %@' exits with code %ld: %@",
                  serviceTarget, plistFilePath, status, [error.userInfo objectForKey:NSLocalizedDescriptionKey]);
        }
        return NO;
    }

    return YES;
}


- (BOOL)unloadJob:(LCJobConfig *)config error:(out NSError *__autoreleasing _Nullable *)errorPtr
{
    uid_t uid = getuid();
    NSString *domainTarget = [NSString stringWithFormat:@"gui/%d", uid];
    NSString *plistFilePath = config.filename;

    NSInteger status = [LCLaunchControl launchCtl:LCBootoutCommand arguments:@[domainTarget, plistFilePath]];
    if (0 != status) {
        NSError *error = [LCLaunchControl launchCtlErrorWithCode:status];
        if (nil != errorPtr) {
            *errorPtr = error;
        } else {
            NSLog(@"Error: 'launchctl bootout %@ %@' exits with code %ld: %@",
                  domainTarget, plistFilePath, status, [error.userInfo objectForKey:NSLocalizedDescriptionKey]);
        }
        return NO;
    }

    return YES;
}


#pragma mark - private methods


+ (NSInteger)launchCtl:(LCCommandName)subcommand arguments:(NSArray<NSString *> *)args
{
    NSArray<NSString *> *launchCtlArgs = [@[subcommand] arrayByAddingObjectsFromArray:args];
#if MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_13
    NSTask *launchCtlTask = [NSTask launchedTaskWithExecutableURL:[NSURL fileURLWithPath:LCLaunchControlPath]
                                                        arguments:launchCtlArgs
                                                            error:nil
                                               terminationHandler:nil];
#else
    NSTask *launchCtlTask = [NSTask launchedTaskWithLaunchPath:LCLaunchControlPath
                                                     arguments:launchCtlArgs];
#endif

    [launchCtlTask waitUntilExit];  /* synchronous call, but starting, stopping jobs etc. should run asynchronously. */
    int exitCode = launchCtlTask.terminationStatus;
    if (0 != exitCode) {
        NSString *a = [launchCtlArgs componentsJoinedByString:@" "];
        NSLog(@"Error: 'launchctl %@' exits with code: %d", a, exitCode);
    }

    return exitCode;
}


+ (NSInteger)launchCtl:(LCCommandName)subcommand arguments:(NSArray<NSString *> *)args returning:(out NSData **)data error:(out NSError **)errorPtr
{
    NSArray<NSString *> *launchCtlArgs = [@[subcommand] arrayByAddingObjectsFromArray:args];

    NSTask *launchCtlTask = NSTask.new;
    launchCtlTask.arguments = launchCtlArgs;
    launchCtlTask.standardOutput = NSPipe.pipe;

    NSError *error = nil;
#if MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_13
    launchCtlTask.executableURL = [NSURL fileURLWithPath:LCLaunchControlPath isDirectory:NO];
    BOOL taskEndsSuccessfully = [launchCtlTask launchAndReturnError:&error];
    NSInteger exitCode = error.code;
#else
    launchCtlTask.launchPath = LCLaunchControlPath;
    [launchCtlTask launch];
    [launchCtlTask waitUntilExit];  /* synchronous call, but starting, stopping jobs etc. should run asynchronously. */

    int exitCode = launchCtlTask.terminationStatus;
    BOOL taskEndsSuccessfully = 0 == exitCode;
    if (!taskEndsSuccessfully) {
        error = [self launchCtlErrorWithCode:exitCode];
    }

#endif

#if 0
    // asyncronous implementation example
    [[launchCtlTask.standardOutput fileHandleForReading] setReadabilityHandler:^(NSFileHandle *file) {
        NSData *data = [file availableData]; // this will read to EOF, so call only once
        NSLog(@"Task output! %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);

        // if you're collecting the whole output of a task, you may store it on a property
        [self.taskOutput appendData:data];
    }];
    /*
     IMPORTANT:
     When your task terminates, you have to set readabilityHandler block to nil; otherwise, you'll encounter high CPU usage, as the reading will never stop.
     */
    [launchCtlTask setTerminationHandler:^(NSTask *task) {

        // do your stuff on completion

        [task.standardOutput fileHandleForReading].readabilityHandler = nil;
        [task.standardError fileHandleForReading].readabilityHandler = nil;
    }];
#endif

    if (!taskEndsSuccessfully) {
        if (nil != errorPtr) {
            *errorPtr = error;
        } else {
            NSString *a = [launchCtlArgs componentsJoinedByString:@" "];
            NSLog(@"Error: 'launchctl %@' exits with code %ld: %@", a, exitCode, [error.userInfo objectForKey:NSLocalizedDescriptionKey]);
        }

        return exitCode;
    }

    NSFileHandle *outputFH = [launchCtlTask.standardOutput fileHandleForReading];
    *data = outputFH.readDataToEndOfFile;
    if (nil != errorPtr) {
        *errorPtr = nil;
    }

    return exitCode;
}


+ (NSError *)launchCtlErrorWithCode:(NSInteger)errorCode
{
    NSData *errorData = nil;
    [self launchCtl:LCErrorCommand arguments:@[@"posix", [NSNumber numberWithInteger:errorCode].stringValue] returning:&errorData error:nil];
    NSDictionary<NSErrorUserInfoKey, id> *errorUserInfo = @{
                                                            NSLocalizedDescriptionKey: [[NSString alloc] initWithData:errorData encoding:NSUTF8StringEncoding]
                                                            };
    NSError *error = [NSError errorWithDomain:NSPOSIXErrorDomain code:errorCode userInfo:errorUserInfo];
    return error;
}


#pragma mark - private parser methods parsing the output of "launchctl list <label>"


- (id)parseLaunchCtrlOutput:(NSString *)outputString
{
    NSScanner *scanner = [NSScanner scannerWithString:outputString];
    return [self parseContainer:scanner];
}


- (id)parseContainer:(NSScanner *)scanner
{
    static NSString * const startContainerCharacters = @"{(";
    static NSString * const endContainerCharacters = @"})";

    id container = nil;

    NSString *startingContainerTag = nil;
    BOOL foundContainer = [scanner scanCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:startContainerCharacters] intoString:&startingContainerTag];
    if (foundContainer) {
        const NSUInteger containerType = [startContainerCharacters rangeOfString:startingContainerTag].location;
        switch (containerType) {
            case 0:
                container = [self parseDictionary:scanner];
                break;
            case 1:
                container = [self parseArray:scanner];
                break;

            default:
                // perhaps parse types of string or other primitive types
                container = nil;
                break;
        }

        NSString *endingContainerTag = nil;
        BOOL foundEnd = [scanner scanCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:endContainerCharacters] intoString:&endingContainerTag];
        if (foundEnd) {
            if (containerType != [endContainerCharacters rangeOfString:endingContainerTag].location) {
                NSLog(@"Error: There's no matching closing container tag.");
            }
            scanner.scanLocation++; // skip the semicolon at end
        } else {
            // TODO: End not found...???
        }
    }

    return container;
}


- (NSDictionary<NSString *, id> *)parseDictionary:(NSScanner *)scanner
{
    // scan an dictionary -> Syntax: '"key" = value;'

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];

    do {
        NSString *key = [self parseKey:scanner];
        if (nil == key) {
            break;
        }

        id value = [self parseValue:scanner];
        if (nil != value) {
            [dict setObject:value forKey:key];
        }
    } while (![scanner scanString:@"};" intoString:nil]);
    scanner.scanLocation -= 2;  /* push back the closing tag, so it can be checked in parseContainer: later */

    return [NSDictionary dictionaryWithDictionary:dict];
}


- (NSArray<id> *)parseArray:(NSScanner *)scanner
{
    // scan an array    -> Syntax: "value;"

    NSMutableArray *array = [NSMutableArray array];

    do {
        id value = [self parseValue:scanner];
        if (nil == value) {
            break;
        }
        [array addObject:value];
    } while (![scanner scanString:@");" intoString:nil]);
    scanner.scanLocation -= 2;  /* push back the closing tag, so it can be checked in parseContainer: later */

    return [NSArray arrayWithArray:array];
}


- (NSString *)parseConstant:(NSScanner *)scanner
{
    // a text which is not enclosed in quotes
    NSString *value = nil;
    if ([scanner scanUpToString:@";" intoString:&value]) {
        scanner.scanLocation++; // skip the semicolon at end
    }
    return value;
}


- (NSString *)parseString:(NSScanner *)scanner
{
    NSString *value = nil;
    if ([scanner scanString:@"\"" intoString:nil] && [scanner scanUpToString:@"\";" intoString:&value]) {
        scanner.scanLocation += 2; // skip the closing quote and semicolon at end
    }
    return value;
}


- (NSNumber *)parseInteger:(NSScanner *)scanner
{
    NSInteger integerValue;
    if (![scanner scanInteger:&integerValue]) {
        return nil;
    }
    scanner.scanLocation++; // skip the semicolon at end
    return [NSNumber numberWithInteger:integerValue];
}


- (NSNumber *)parseBoolean:(NSScanner *)scanner
{
    if ([scanner scanString:@"true" intoString:nil]) {
        scanner.scanLocation++; // skip the semicolon at end
        return [NSNumber numberWithBool:YES];
    }

    if ([scanner scanString:@"false" intoString:nil]) {
        scanner.scanLocation++; // skip the semicolon at end
        return [NSNumber numberWithBool:NO];
    }

    return nil;
}


- (NSString *)parseKey:(NSScanner *)scanner
{
    NSString *key = nil;
    if ([scanner scanString:@"\"" intoString:nil] && [scanner scanUpToString:@"\" = " intoString:&key]) {
        scanner.scanLocation += 4;
    }
    return key;
}


- (id)parseValue:(NSScanner *)scanner
{
    id value = [self parseInteger:scanner];
    if (nil != value) {
        return value;
    }

    value = [self parseBoolean:scanner];
    if (nil != value) {
        return value;
    }

    value = [self parseString:scanner];
    if (nil != value) {
        return value;
    }

    value = [self parseContainer:scanner];
    if (nil != value) {
        return value;
    }

    value = [self parseConstant:scanner];
    return value;
}

@end
