//
//  HWArrayMapping.m
//  LaunchCenterPrefPane
//
//  Created by henrik on 31.07.18.
//  Copyright © 2018 hackmac. All rights reserved.
//

#import "HWArrayMapping.h"


@implementation NSArray(HWArrayMapping)

+ (instancetype)arrayWithArray:(NSArray<id> *)array usingMapper:(HWMapperBlock)mapperBlock
{
    return [[NSArray alloc] initWithArray:array usingMapper:mapperBlock];
}


- (instancetype)initWithArray:(NSArray<id> *)array usingMapper:(HWMapperBlock)mapperBlock
{
    self = [self initWithArray:array usingMapper:mapperBlock filerUsingPredicate:[NSPredicate predicateWithValue:YES]];
    return self;
}


+ (instancetype)arrayWithArray:(NSArray<id> *)array usingMapper:(HWMapperBlock)mapperBlock filerUsingPredicate:(NSPredicate *)predicate
{
    return [[NSArray alloc] initWithArray:array usingMapper:mapperBlock filerUsingPredicate:predicate];
}


- (instancetype)initWithArray:(NSArray<id> *)array usingMapper:(HWMapperBlock)mapperBlock filerUsingPredicate:(NSPredicate *)predicate
{
    NSMutableArray<id> *tempArray = [NSMutableArray arrayWithCapacity:array.count];
    [array enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        id mappedObject = mapperBlock(obj);
        if ([predicate evaluateWithObject:mappedObject]) {
            [tempArray addObject:mappedObject];
        }
    }];

    self = [self initWithArray:tempArray];
    return self;
}


- (nonnull NSArray<id> *)mappedArrayUsingBlock:(HWMapperBlock)mapper
{
    return [NSArray arrayWithArray:self usingMapper:mapper];
}


- (nonnull NSArray<id> *)mappedArrayUsingBlock:(HWMapperBlock)mapper andFilterUsingPredicate:(NSPredicate *)predicate
{
    return [NSArray arrayWithArray:self usingMapper:mapper filerUsingPredicate:predicate];
}


- (nonnull NSArray<id> *)filteredArrayUsingPredicate:(NSPredicate *)predicate mappingObjects:(HWMapperBlock)mapperBlock
{
    NSMutableArray<id> *tempArray = [NSMutableArray arrayWithCapacity:self.count];
    [self enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([predicate evaluateWithObject:obj]) {
            [tempArray addObject:mapperBlock(obj)];
        }
    }];

    return [tempArray copy];  // just for an imutable version of the new array
}

@end
