//
//  HWArrayMapping.h
//  LaunchCenterPrefPane
//
//  Created by henrik on 31.07.18.
//  Copyright © 2018 hackmac. All rights reserved.
//

#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@interface NSArray<ObjectType> (HWArrayMapping)

typedef id _Nonnull (^HWMapperBlock)(ObjectType _Nonnull obj);

+ (instancetype)arrayWithArray:(NSArray<id> *)array usingMapper:(HWMapperBlock)block;
- (instancetype)initWithArray:(NSArray<id> *)array usingMapper:(HWMapperBlock)block;

+ (instancetype)arrayWithArray:(NSArray<id> *)array usingMapper:(HWMapperBlock)block filerUsingPredicate:(NSPredicate *)predicate;
- (instancetype)initWithArray:(NSArray<id> *)array usingMapper:(HWMapperBlock)block filerUsingPredicate:(NSPredicate *)predicate;

/// converts all recievers objects into another object and return a mapped array
- (NSArray<id> *)mappedArrayUsingBlock:(HWMapperBlock)mapper;
/// map every recievers object and evaluate a predicate against it and return a mapped, filtered array
- (NSArray<id> *)mappedArrayUsingBlock:(HWMapperBlock)mapper andFilterUsingPredicate:(NSPredicate *) predicate;

/// returns a mapped array of all filtered objects in the reciever
- (NSArray<id> *)filteredArrayUsingPredicate:(NSPredicate *)predicate mappingObjects:(HWMapperBlock)mapperBlock;

@end

NS_ASSUME_NONNULL_END
