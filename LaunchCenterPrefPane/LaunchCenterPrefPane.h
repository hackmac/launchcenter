//
//  LaunchCenterPrefPane.h
//  LaunchCenterPrefPane
//
//  Created by henrik on 26.07.18.
//  Copyright © 2018 hackmac. All rights reserved.
//

#import <PreferencePanes/PreferencePanes.h>


@interface LaunchCenterPrefPane : NSPreferencePane <NSOutlineViewDataSource, NSOutlineViewDelegate>

@property (readonly) NSUserDefaultsController *sharedUserDefaultsController;

- (IBAction)showProgramPathErrorAction:(id)sender;
- (IBAction)showPreviewAction:(id)sender;
- (IBAction)showInFinderAction:(id)sender;

- (IBAction)startStopJob:(id)sender;
- (IBAction)loadUnloadJob:(id)sender;

- (IBAction)showFindBar:(id)sender;
- (IBAction)searchJobByLabel:(NSSearchField *)sender;

- (IBAction)selectPathForExecutable:(NSPathControl *)sender;

- (IBAction)endEditingTextField:(id)sender;

@end
